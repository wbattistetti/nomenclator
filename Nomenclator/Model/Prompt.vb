﻿Public Class Prompt
    Public Enum Types
        Item
        StartAutomatic
        StartManual
        NoMatch1
        NoMatch2
        Rendering
    End Enum

    Private _texts(Types.Rendering) As String
    Public Property Texts() As String()
        Get
            Return _texts
        End Get
        Set(value As String())
            _texts = value
        End Set
    End Property


End Class
