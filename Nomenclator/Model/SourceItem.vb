﻿Public Class SourceItem
    Public Property Text As String

    Public Property ParseResult As String
    ''' <summary>
    ''' The list of values for addtional fields.
    ''' </summary>
    Public Property AdditionalValues As New List(Of String)

    Public Sub New()
    End Sub

    Public Sub New(text As String)
        Me.Text = text
    End Sub
End Class
