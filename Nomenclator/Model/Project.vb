﻿Imports System.ComponentModel

Public Class Project

    Public Path As String

    Public Result(ParsingStates.Completed) As Integer
    Public Property Nomenclature As New Nomenclature
    Public Property Vocabularies As New BindingList(Of Vocabulary)
    Public Property Formats As String
    Public Property Clauses As New List(Of FormatClause)

    Public Property ParsedField As Nomenclature.Field
    Public Property Prompts As New List(Of Prompt)

    ''' <summary>
    ''' The list of fields to be included in the export table, separated by commas or semicolons
    ''' </summary>>
    Public Property ExportFields As String

End Class
