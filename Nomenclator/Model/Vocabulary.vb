﻿Public Class Vocabulary

    Public Class Rendering
        Public Property Filler As String
        Public Property Term As String
        Public Property ColloquialTerm As String

        Public Sub New(term As String, filler As String, colloquialTerm As String)
            Me.Term = term
            Me.Filler = filler
            Me.ColloquialTerm = colloquialTerm
        End Sub
    End Class

    Public Class Synonym
        Public Property MainTerm As String
        Public Property Terms As New List(Of String)

        Public Sub New(mainTerm As String)
            Me.MainTerm = mainTerm
        End Sub

        Public Sub New(mainTerm As String, terms As List(Of String))
            Me.MainTerm = mainTerm
            Me.Terms = terms
        End Sub
    End Class

    Public Property Name As String
    Public Property Words As New List(Of String)
    Public Property Renderings As New List(Of Rendering)
    Public Property Synonyms As New List(Of Synonym)
    Public Property Pattern As String

    Public Sub New(name As String)
        Me.Name = name
    End Sub
End Class
