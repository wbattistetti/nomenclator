﻿Public Class ItemRow

    ''' <summary>
    ''' For each format defined, find all the matches for the cell 
    ''' </summary>
    Public Class ClauseMatch
        Public Property Clause As FormatClause
        Public Property Words As New List(Of WordMatch)
        Public Property UnmatchedText As String

        Public Sub New()
        End Sub
    End Class

    ''' <summary>
    ''' The word matched in the cell text and the vocabulary that contains that word
    ''' </summary>
    Public Class WordMatch
        Public Property Value As String
        Public Property VocName As String

        ''' <summary>
        ''' The start position of the match in the cell text
        ''' </summary>
        Public Property StartPosition As Integer

        Public Sub New(value As String, vocName As String, position As Integer)
            Me.Value = value
            Me.VocName = vocName
            Me.StartPosition = position
        End Sub
    End Class

    Public Class Cell
        Public Property FieldName As String
        Public Property Text As String
        Public Property ParsedText As String
        Public Property UnparsedText As String

        Public Sub New(fieldName As String, text As String)
            Me.FieldName = fieldName
            Me.Text = text
        End Sub

        ''' <summary>
        ''' The list of substrings found in the cell text that match a word in the vocabularies
        ''' </summary>
        Public Property Matches As List(Of ClauseMatch)
        Public Property WinnerClause As ClauseMatch
    End Class

    Public Property State As ParsingStates

    Public Property Cells As New List(Of Cell)
    Public Property ParseResult As String
    Public Property HidingParseResults As New List(Of String)
    Public Property UnparsedText As String
End Class
