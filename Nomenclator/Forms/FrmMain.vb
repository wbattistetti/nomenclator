﻿Imports System.Text
Imports System.Text.RegularExpressions
Imports Ookii.Dialogs.WinForms

Public Class FrmMain
    Private Structure Colors
        Public Parsed As Color
        Public Ok As Color
        Public Ambiguous As Color
        Public UnMatched As Color
        Public Unique As Color
        Public Underspecified As Color
        Public Counter As Color
        Public Excluded As Color
    End Structure

    Public Class RowView
        Public Property GridRow As DataGridViewRow
        Public Property GridRowIndex As Integer
        Public Property ItemRow As ItemRow

        Public Sub New(gridRow As DataGridViewRow, itemRow As ItemRow)
            Me.GridRow = gridRow
            Me.ItemRow = itemRow
        End Sub
    End Class

    Friend Enum CopyModes
        Words
        FullText
    End Enum

    Friend Enum Columns
        ''' <summary>The column showing if a parsing has been approved</summary>
        OK
        ''' <summary>The column showing the result of the parsing</summary>
        Parsed
        ''' <summary>The column showing which part of original text was not matched</summary>
        UnMatched
        ''' <summary>The column showing which original text must be excluded from parsing</summary>
        Excluded
    End Enum

    Private _fieldToBeParsed As Nomenclature.Field = Nothing
    Private Property FieldToBeParsed As Nomenclature.Field
        Get
            Return _fieldToBeParsed
        End Get
        Set(value As Nomenclature.Field)
            _fieldToBeParsed = value
            _project.ParsedField = value
            ShowTabPrompt(value IsNot Nothing)
            If value IsNot Nothing Then OutputTable.BuildToolstripStart()
        End Set
    End Property

    Private _rowViews As New List(Of RowView)
    Private _allRowViews As New List(Of RowView)
    Private _project As New Project
    Private WithEvents _cellSearch As TextBox
    Private WithEvents _cellText As TextBox
    Private _buttonCounters(ParsingStates.ManuallyDesigned) As ToolStripButton
    Private _intellisense As Intellisense
    Private _specialColumnIndex(Columns.Excluded) As Integer
    Private _selectedItemsLabel As ToolStripLabel
    Private _parseButton As ToolStripMenuItem
    Private _fieldToBeParsedButton As ToolStripMenuItem
    Private _tooltip As New ToolTip
    Private _sortedDescending As Boolean
    Private _sortedIndex As Integer
    Private _colors As Colors
    Dim _buttonsDict As Dictionary(Of String, ToolStripButton) = New Dictionary(Of String, ToolStripButton)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        With _colors
            .Ambiguous = Color.DarkOrange
            .Excluded = Color.DarkGray
            .Ok = Color.YellowGreen
            .Parsed = Color.LightBlue
            .UnMatched = Color.LightCoral
        End With

        BuildToolstrip()
        ShowTabPrompt(False)
    End Sub

    Private Sub ShowTabPrompt(show As Boolean)
        If show Then
            TabControl.TabPages.Add(TabPrompts)
        Else
            TabControl.TabPages.Remove(TabPrompts)
        End If
    End Sub

    Public Function ChooseFile(Optional resourceFolder As String = "", Optional title As String = "Choose File",
                               Optional filter As String = "CSV Files (*.wav)|*.csv") As String
        'If String.IsNullOrEmpty(resourceFolder) Then resourceFolder = SpecialDirectories.MyDocuments
        Using dialog = New VistaOpenFileDialog With {
            .InitialDirectory = resourceFolder,
            .Title = title,
            .Filter = filter,
            .Multiselect = False,
            .CheckFileExists = True,
            .CheckPathExists = True,
            .RestoreDirectory = True
        }
            If dialog.ShowDialog() = DialogResult.OK Then
                Return dialog.FileName
            End If
            Return ""
        End Using
    End Function

    ''' <summary>
    ''' Prepares the grid to show the db
    ''' </summary>
    Private Sub SetGrid(fields As IEnumerable(Of Nomenclature.Field), grd As DataGridView)
        With grd
            .BackgroundColor = Color.Black
            .DefaultCellStyle.ForeColor = Color.White
            .DefaultCellStyle.BackColor = Color.Black

            .AllowUserToResizeRows = False
            .Columns.Clear()
            '_columnViews.Clear()
            For Each field In fields
                Dim col = New DataGridViewTextBoxColumn
                '_columnViews.Add(New ColumnView(col))
                col.HeaderText = field.Name
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                col.SortMode = DataGridViewColumnSortMode.NotSortable '   .Automatic   ' if sort column the fixed row lost their position
                col.Tag = field
                .Columns.Add(col)
            Next

            CreateSpecialColumns()
        End With
    End Sub

    Private Sub CreateSpecialColumns()
        Dim col As DataGridViewColumn = Nothing
        For Each c As Columns In System.Enum.GetValues(GetType(Columns))
            Select Case c
                Case Columns.OK, Columns.Excluded
                    col = New DataGridViewCheckBoxColumn
                Case Else
                    col = New DataGridViewTextBoxColumn
            End Select

            Select Case c
                Case Columns.Parsed
                    col.DefaultCellStyle.ForeColor = _colors.Parsed
                Case Columns.UnMatched
                    col.DefaultCellStyle.ForeColor = _colors.UnMatched
            End Select
            col.HeaderText = c.ToString ' EnumUtils(Of Columns).GetDescription(c)
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            col.SortMode = DataGridViewColumnSortMode.Automatic ' if sort collumn the fixed row lost their position

            col.Tag = c
            GrdItems.Columns.Add(col)
            _specialColumnIndex(c) = col.Index
        Next
    End Sub

    Public Sub ShowItems(nomenclature As Nomenclature)
        Me.Cursor = Cursors.WaitCursor
        GrdItems.EndEdit()
        GrdItems.Rows.Clear()
        _rowViews = New List(Of RowView)
        GrdItems.Columns.Clear()
        _project.Nomenclature = nomenclature
        VocabularyEditor.Vocabularies = _project.Vocabularies

        If nomenclature Is Nothing Then Exit Sub
        SetGrid(nomenclature.Fields, GrdItems)
        If Not nomenclature.Fields.Any Then Exit Sub
        Dim rows = New List(Of DataGridViewRow)
        Dim row = New DataGridViewRow
        row.Frozen = True
        row.CreateCells(GrdItems)
        rows.Add(row)

        For Each oRow In nomenclature.Rows.OrderBy(Function(o) o.Cells(0).Text)
            row = New DataGridViewRow
            row.CreateCells(GrdItems)
            Dim col = -1
            For Each cell In oRow.Cells
                col += 1
                row.Cells(col).Value = cell.Text
                row.Cells(col).Tag = cell
            Next
            row.Cells(_specialColumnIndex(Columns.Parsed)).Value = oRow.ParseResult
            row.Cells(_specialColumnIndex(Columns.UnMatched)).Value = oRow.UnparsedText

            'add special cels if missing
            If oRow.Cells.Count = nomenclature.Fields.Count Then
                For Each c As Columns In System.Enum.GetValues(GetType(Columns))
                    oRow.Cells.Add(New ItemRow.Cell(c.ToString, ""))
                Next
            End If
            rows.Add(row)
            _rowViews.Add(New RowView(row, oRow))
        Next

        _allRowViews.AddRange(_rowViews)

        With GrdItems
            .SuspendLayout()
            .Rows.AddRange(rows.ToArray)
            For Each rv In _rowViews
                rv.GridRowIndex = rv.GridRow.Index
            Next
            .ResumeLayout()
        End With
        Me.Cursor = Cursors.Default
    End Sub

    Public Sub ShowItemRows(itemRows As IEnumerable(Of ItemRow))
        Me.Cursor = Cursors.WaitCursor
        Dim rows = New List(Of DataGridViewRow)
        Dim searchRow = GrdItems.Rows(0)
        Dim showingRows = itemRows.ToList
        _rowViews.Clear()
        For Each iRow In showingRows
            Dim row = New DataGridViewRow
            row.CreateCells(GrdItems)
            Dim col = -1
            For Each cell In iRow.Cells
                col += 1
                row.Cells(col).Value = cell.Text
                row.Cells(col).Tag = cell
            Next
            row.Cells(_specialColumnIndex(Columns.Parsed)).Value = iRow.ParseResult
            row.Cells(_specialColumnIndex(Columns.UnMatched)).Value = iRow.UnparsedText
            rows.Add(row)
            _rowViews.Add(New RowView(row, iRow))
        Next

        With GrdItems
            .EndEdit()
            Dim col = .CurrentCell.ColumnIndex
            .SuspendLayout()
            .Rows.Clear()
            .Rows.Add(searchRow)
            .Rows.AddRange(rows.ToArray)
            .ResumeLayout()

            For Each rv In _rowViews
                rv.GridRowIndex = rv.GridRow.Index
            Next
            If _selectedItemsLabel IsNot Nothing Then _selectedItemsLabel.Text = $"#{_rowViews.Count} selected"
            BeginInvoke(Sub()
                            .CurrentCell = GrdItems(col, 0)
                            .BeginEdit(True)
                        End Sub)
        End With
        Me.Cursor = Cursors.Default
    End Sub

    Public Function LoadItems(fileName As String) As Nomenclature
        If String.IsNullOrEmpty(fileName) Then Return Nothing
        Dim nomenclature As New Nomenclature
        Dim objReader As New System.IO.StreamReader(fileName)
        Dim line As String
        line = objReader.ReadLine()  'the header
        For Each m As Match In Regex.Matches(line, "[^;,]+")
            nomenclature.Fields.Add(New Nomenclature.Field(m.Value))
        Next
        Do While objReader.Peek() <> -1
            line = objReader.ReadLine()
            line = Regex.Replace(line, ";", "; ")
            Dim mcoll = Regex.Matches(line, "[^;,]+")
            Dim oRow = New ItemRow()
            If Not String.IsNullOrEmpty(line) Then
                For i = 0 To mcoll.Count - 1
                    If i < nomenclature.Fields.Count Then
                        oRow.Cells.Add(New ItemRow.Cell(nomenclature.Fields(i).Name, mcoll(i).Value))
                    End If
                Next
                nomenclature.Rows.Add(oRow)
            End If
        Loop
        objReader.Close()
        Return nomenclature
    End Function

    Private Sub BuildToolstrip()
        Dim mFile = New ToolStripMenuItem("File")

        Dim mNew = New ToolStripMenuItem("New Project", My.Resources.newitem,
                                          Sub()
                                              GrdItems.Rows.Clear()
                                              VocabularyEditor.Clear()
                                              FormatEditor.Clear()
                                              _project = New Project
                                              FormatEditor.Project = _project
                                              OutputTable.Project = _project
                                              ShowItems(LoadItems(ChooseFile(, "Load Items", "Ontology CSV (*.csv)|*.csv")))
                                          End Sub)

        Dim mLoad = New ToolStripMenuItem("Load", My.Resources.folderopen)

        Dim mLoadProject = New ToolStripMenuItem("Project", My.Resources.folderopen,
                                          Sub()
                                              GrdItems.Rows.Clear()
                                              VocabularyEditor.Clear()
                                              FormatEditor.Clear()
                                              Dim prj = Loader.LoadProjectFile(ChooseFile(, "Load Project", "Nomenclature Projects (*.Nomenclature)|*.Nomenclature"))
                                              If prj Is Nothing Then Exit Sub  'abort of the loading
                                              _project = prj
                                              OutputTable.Project = _project
                                              ShowItems(_project.Nomenclature)
                                              BuildToolstripFields()
                                              SetParseField(_project.ParsedField)
                                              _parseButton.Visible = prj.ParsedField IsNot Nothing
                                              FormatEditor.Project = _project
                                              VocabularyEditor.Vocabularies = _project.Vocabularies
                                          End Sub)

        Dim mLoadItems = New ToolStripMenuItem("Items", My.Resources.folderopen,
                                          Sub()
                                              'GrdItems.Rows.Clear()
                                              _project.Nomenclature.Rows.Clear()
                                              Dim nmk = LoadItems(ChooseFile(, "Load Items", "Ontology CSV (*.csv)|*.csv"))
                                              If nmk IsNot Nothing Then
                                                  _project.Nomenclature = nmk
                                                  ShowItems(_project.Nomenclature)
                                                  MenuFieldToParse()
                                                  BuildToolstripFields()
                                              End If
                                          End Sub)

        Dim mAddItems = New ToolStripMenuItem("Add Items", My.Resources.folderopen,
                                          Sub()
                                              Dim newNmk = LoadItems(ChooseFile(, "Load Items", "Ontology CSV (*.csv)|*.csv"))
                                              If newNmk Is Nothing Then Exit Sub
                                              If newNmk.Fields.Any(Function(f) Not _project.Nomenclature.Fields.Any(Function(f1) f.Name.ToLower.Trim = f1.Name.ToLower.Trim)) OrElse
                                                  _project.Nomenclature.Fields.Any(Function(f) Not newNmk.Fields.Any(Function(f1) f.Name.ToLower.Trim = f1.Name.ToLower.Trim)) Then
                                                  MessageBox.Show("The items are not compatible")
                                                  Exit Sub
                                              Else
                                                  _project.Nomenclature.Rows.AddRange(newNmk.Rows)
                                              End If
                                              ShowItems(_project.Nomenclature)
                                              MenuFieldToParse()
                                          End Sub)

        Dim mSave = New ToolStripMenuItem("Save", My.Resources.Save,
                                          Sub()
                                              OutputTable.Save()
                                              SetProjectPath()
                                              If Not String.IsNullOrEmpty(_project.Path) Then
                                                  Saver.SaveProjectFile(_project, _project.Path)
                                              End If
                                          End Sub)
        Dim mSaveAs = New ToolStripMenuItem("Save As", My.Resources.Save, Sub()
                                                                              Saver.SaveProjectFile(_project, _project.Path)
                                                                          End Sub)

        Dim mUpdateVocs = New ToolStripMenuItem("Update Vocabularies", My.Resources.Vocabulary,
                                                Sub()
                                                    VocabularyEditor.UpdateVocabularyList(FormatEditor.RTFormats.Text)
                                                    FormatEditor.RTFormats.Visible = False
                                                    FormatEditor.FormatText()
                                                    FormatEditor.RTFormats.Visible = True
                                                End Sub)


        mLoad.DropDownItems.Add(mLoadProject)
        mLoad.DropDownItems.Add(mLoadItems)

        mFile.DropDownItems.Add(mNew)
        mFile.DropDownItems.Add("-")
        mFile.DropDownItems.Add(mLoad)
        mFile.DropDownItems.Add("-")
        mFile.DropDownItems.Add(mAddItems)
        mFile.DropDownItems.Add("-")
        mFile.DropDownItems.Add(mSave)
        mFile.DropDownItems.Add(mSaveAs)
        mFile.DropDownItems.Add("-")
        ToolStrip.Items.Add(mFile)
        ToolStrip.Items.Add(mUpdateVocs)
    End Sub

    Private Sub SetProjectPath()
        If String.IsNullOrEmpty(_project.Path) Then
            Dim SaveFileDialog = New SaveFileDialog
            SaveFileDialog.Filter = "Nomenclature Files (*.Nomenclature)|*.Nomenclature"
            If SaveFileDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                _project.Path = SaveFileDialog.FileName
            End If
        End If
    End Sub

    Private Sub CopyWords(colIndex As Integer, copyMode As CopyModes)
        Dim words = New List(Of String)
        Dim values As IEnumerable(Of String)
        If GrdItems.SelectedCells.Count > 1 Then
            values = _rowViews.Where(Function(r) r.GridRow.Cells(colIndex).Selected).
                      Select(Function(r) r.GridRow.Cells(colIndex).Value?.ToString)
        Else
            values = _rowViews.Select(Function(r) r.GridRow.Cells(colIndex).Value.ToString)
        End If

        Select Case copyMode
            Case CopyModes.Words
                For Each value In values.Where(Function(v) Not String.IsNullOrEmpty(v.ToString)).
                                                           OrderBy(Function(v) v)


                    words.AddRange(value.GetWords)
                Next
            Case CopyModes.FullText
                For Each value In values.Where(Function(v) Not String.IsNullOrEmpty(v.ToString)).
                                                           OrderBy(Function(v) v)

                    words.Add(value)
                Next
        End Select
        Dim sb = New StringBuilder
        For Each word In words.Distinct.OrderBy(Function(w) w)
            sb.AppendLine(word)
        Next
        VocabularyEditor.ClipboardBox = sb.ToString
        VocabularyEditor.LblClip1.Text = $"Clpiboard 1 #{words.Count}"
    End Sub

    Private Sub MenuCellColumnParsed(filedName As String, cell As DataGridViewCell)
        Dim menu = New ToolStripDropDownMenu
        menu.Items.Add("Edit Manually", My.Resources.edit, Sub()
                                                               GrdItems.BeginEdit(True)
                                                           End Sub)
        menu.Show(MousePosition)
    End Sub

    Private Sub MenuCell(fieldName As String, cell As DataGridViewCell, Optional selectedText As String = "")
        Dim menu = New ToolStripDropDownMenu
        Dim mWords = New ToolStripMenuItem("Extract All Words", Nothing,
                                                  Sub()
                                                      CopyWords(cell.ColumnIndex, CopyModes.Words)
                                                  End Sub)

        mWords.ToolTipText = "Extract all the words found in the selected cells"

        Dim mFullCell = New ToolStripMenuItem("Copy Cell Text", Nothing,
                                                  Sub()
                                                      CopyWords(cell.ColumnIndex, CopyModes.FullText)
                                                  End Sub)
        mFullCell.ToolTipText = "Copy all the cell text selected"

        menu.Items.Add(mWords)
        menu.Items.Add(mFullCell)
        menu.Items.Add(MenuFindWord(selectedText))

        If FieldToBeParsed Is Nothing OrElse cell.OwningColumn.HeaderText = FieldToBeParsed.Name Then
            Dim mParseAll = New ToolStripMenuItem("Parse All", Nothing,
                                                  Sub()
                                                      If FieldToBeParsed Is Nothing Then
                                                          _fieldToBeParsedButton.ShowDropDown()
                                                      Else
                                                          ParseAll()
                                                      End If
                                                  End Sub)
            mParseAll.ToolTipText = "Parse selected column with all the vocabularies defined"

            Dim mParseSingleLine = New ToolStripMenuItem("Parse Cell", Nothing,
                                                         Sub()
                                                             If FieldToBeParsed Is Nothing Then
                                                                 _fieldToBeParsedButton.ShowDropDown()
                                                             Else
                                                                 ParseCell(fieldName, cell)
                                                             End If
                                                         End Sub)
            mParseSingleLine.ToolTipText = "Parse selected cell with all the vocabularies defined"

            menu.Items.Add(mParseAll)
            If cell?.RowIndex > -1 Then menu.Items.Add(mParseSingleLine)

            menu.Items.Add("Delete", My.Resources.delete,
                           Sub()
                               Dim r = _rowViews.Single(Function(rv) rv.GridRow.Index = cell.RowIndex)
                               _project.Nomenclature.Rows.Remove(r.ItemRow)
                               _rowViews.Remove(r)
                               GrdItems.Rows.Remove(GrdItems.Rows(cell.RowIndex))
                           End Sub)


            If cell IsNot Nothing AndAlso CellsDuplicated(cell.ColumnIndex) Then
                menu.Items.Add("Remove Duplications", Nothing,
                           Sub()
                               RemoveDuplications(cell.ColumnIndex)
                           End Sub)
            End If

            If cell?.OwningColumn.HeaderText = FieldToBeParsed.Name Then
                menu.Items.Add("Import From Clipbord", My.Resources.CopyToClipboard,
                           Sub()
                               Dim itemRow = _project.Nomenclature.Rows.First
                               For Each m As Match In Regex.Matches(Clipboard.GetText, "[^\r\n]+")
                                   Dim newItemRow = New Nomenclator.ItemRow
                                   For Each rowCell In itemRow.Cells
                                       newItemRow.Cells.Add(New ItemRow.Cell(rowCell.FieldName, If(rowCell.FieldName = FieldToBeParsed.Name, m.Value.Trim, "")))
                                   Next
                                   _project.Nomenclature.Rows.Add(newItemRow)
                               Next
                               ShowItems(_project.Nomenclature)
                           End Sub)
            End If

        End If
            menu.Show(MousePosition)
    End Sub

    Private Function CellsDuplicated(colIndex As Integer) As Boolean
        Dim distinctValues = _rowViews.Select(Function(rv) rv.GridRow.Cells(colIndex).Value?.ToString).Distinct
        Return distinctValues.Count < _rowViews.Count
    End Function

    Private Sub RemoveDuplications(colIndex As Integer)
        Dim distinctValues = _rowViews.Select(Function(rv) rv.GridRow.Cells(colIndex).Value?.ToString).Distinct
        Dim rowViewsToRemove = New List(Of RowView)
        For Each value In distinctValues
            Dim rows = _rowViews.Where(Function(rv) rv.GridRow.Cells(colIndex).Value?.ToString.ToLower = value.ToLower.Trim)
            If rows.Count > 1 Then
                rowViewsToRemove.AddRange(rows.Skip(1))
            End If
        Next
        For Each rv In rowViewsToRemove
            _project.Nomenclature.Rows.Remove(rv.ItemRow)
        Next
        ShowItemRows(_project.Nomenclature.Rows)
    End Sub

    Private Function MenuFindWord(selectedText As String) As ToolStripMenuItem
        Dim mFindWord = New ToolStripMenuItem($"""{selectedText.Trim.ToUpper}"" in vocabulary ?")
        mFindWord.ToolTipText = "Click t discover"

        If Not String.IsNullOrEmpty(selectedText.Trim) Then
            For Each voc In _project.Vocabularies.OrderBy(Function(v) v.Name)
                Dim mVoc = New ToolStripMenuItem(voc.Name, My.Resources.Vocabulary,
                                            Sub()
                                                If Not voc.Words.Any(Function(w) w.ToLower = selectedText.ToLower.Trim) Then
                                                    voc.Words.Add(selectedText.Trim)
                                                    VocabularyEditor.RtBox.Text = $"{VocabularyEditor.RtBox.Text}{Environment.NewLine}{selectedText.Trim}"
                                                End If
                                            End Sub)
                If voc.Words.Any(Function(w) w.ToLower = selectedText.ToLower.Trim) Then
                    mVoc.Checked = True
                    mVoc.Font = New Font(mVoc.Font.FontFamily, mVoc.Font.Size, FontStyle.Bold)
                    mVoc.DropDownItems.Add("Open", My.Resources.edit, Sub()

                                                                      End Sub)
                Else
                    mVoc.Font = New Font(mVoc.Font.FontFamily, mVoc.Font.Size, FontStyle.Regular)
                    mVoc.DropDownItems.Add("Add", My.Resources.edit, Sub()

                                                                     End Sub)
                End If
            Next
        Else
            mFindWord.Visible = False
        End If
        AddHandler mFindWord.MouseEnter,
                Sub()
                    mFindWord.DropDownItems.Clear()
                    For Each vocabulary In _project.Vocabularies.Where(Function(v) v.Words.Any(Function(w) w.ToLower = selectedText.Trim.ToLower))
                        mFindWord.DropDownItems.Add(vocabulary.Name, My.Resources.Vocabulary,
                                                                                         Sub()

                                                                                         End Sub)
                    Next
                    If mFindWord.DropDownItems.Count = 0 Then
                        mFindWord.Text = $"""{selectedText.Trim.ToUpper}"" not in vocabulary!... "
                        Dim mAdd = New ToolStripMenuItem("Add In...")
                        For Each voc In _project.Vocabularies
                            mAdd.DropDownItems.Add(voc.Name, My.Resources.Vocabulary,
                                                   Sub()
                                                       VocabularyEditor.AddWord(voc, selectedText)
                                                   End Sub)
                        Next
                        mFindWord.DropDownItems.Add(mAdd)
                    Else
                        mFindWord.Text = $"""{selectedText.Trim.ToUpper}"" in vocabulary:"
                    End If
                End Sub

        Return mFindWord
    End Function

    Private Sub ParseAll()
        'If Not GrdItems.Rows(0).Visible Then

        'End If
        Dim sbPattern = New StringBuilder
        VocabularyEditor.CompileVocabularies()
        _project.Clauses = FormatEditor.GetFormatClauses(_project.Vocabularies).ToList
        GrdItems.Rows.Clear()
        For Each rowView In _rowViews
            ParseRowView(rowView)
        Next

        SetParseState()

        GrdItems.Rows.AddRange(_rowViews.Select(Function(rv) rv.GridRow).ToArray)
        Dim row = New DataGridViewRow
        row.Frozen = True
        GrdItems.Rows.Insert(0, row)
    End Sub

    Private Sub SetParseState()
        Dim rowsWithParseResults = _rowViews.Where(Function(r) Not (r.ItemRow.State = ParsingStates.Excluded OrElse
                                         String.IsNullOrEmpty(r.ItemRow.ParseResult)))
        For Each rv1 In rowsWithParseResults
            If rowsWithParseResults.Except({rv1}).Any(Function(r) r.ItemRow.ParseResult = rv1.ItemRow.ParseResult) Then
                rv1.ItemRow.State = ParsingStates.Ambiguous
            Else
                rv1.ItemRow.State = ParsingStates.Unique
            End If
        Next

        'search under specified
        Dim rowsUnique = rowsWithParseResults.Where(Function(r) r.ItemRow.State = ParsingStates.Unique)
        For Each rv1 In rowsUnique
            Dim hidings = rowsUnique.Except({rv1}).Where(Function(r) Strings.Left(r.ItemRow.ParseResult, rv1.ItemRow.ParseResult.Length) = rv1.ItemRow.ParseResult).
                Select(Function(r) r.ItemRow.ParseResult)
            If hidings.Any Then
                rv1.ItemRow.HidingParseResults.Clear()
                rv1.ItemRow.HidingParseResults.AddRange(hidings)
            End If
            If rowsUnique.Except({rv1}).Any(Function(r) Strings.Left(r.ItemRow.ParseResult, rv1.ItemRow.ParseResult.Length) = rv1.ItemRow.ParseResult) Then
                rv1.ItemRow.State = ParsingStates.Underspecified
            End If
        Next


        Dim parsedCount = _rowViews.Count
        Dim count = 0
        For Each counter As ParsingStates In System.Enum.GetValues(GetType(ParsingStates))
            Select Case counter
                Case ParsingStates.Completed
                    count = _rowViews.Where(Function(r) r.ItemRow.State = ParsingStates.Unique).Count

                Case ParsingStates.UnMatched
                    count = _rowViews.Where(Function(r) String.IsNullOrEmpty(r.ItemRow.ParseResult)).Count

                Case ParsingStates.Underspecified
                    count = _rowViews.Where(Function(r) r.ItemRow.State = ParsingStates.Underspecified).Count

                Case Else
                    count = _rowViews.Where(Function(r) r.ItemRow.State = counter).Count
            End Select
            _buttonCounters(counter).Text = $"{counter.ToString} {count} ({Format(count / parsedCount * 100, "0")}%)"

            If counter = ParsingStates.Completed Then
                _buttonCounters(ParsingStates.Completed).ForeColor = If(count = parsedCount, Color.Green, Color.Red)
            End If
        Next

    End Sub

    Private Sub ParseCell(fieldName As String, cell As DataGridViewCell)
        VocabularyEditor.UpdateVocabulary()
        VocabularyEditor.CompileVocabularies()
        _project.Clauses = FormatEditor.GetFormatClauses(_project.Vocabularies).ToList
        Dim rowView = _rowViews.SingleOrDefault(Function(rv) rv.GridRowIndex = cell.RowIndex)
        rowView = ParseRowView(rowView)
        SetParsedCell(rowView.ItemRow.Cells.Single(Function(c) c.FieldName = cell.OwningColumn.HeaderText), rowView)
    End Sub

    ''' <summary>
    ''' Given a cell.text find all the matches of the words contained in the vocabulary in the order defined in format
    ''' For example:
    ''' 
    ''' RX polso mano sx  -->   RX.Mano.Polso.Sx 
    ''' 
    ''' FormatLine is a list of dictionary name. 
    ''' For example.
    '''      Type.BodyMain.BodyDetail.Part
    '''      
    '''      RX polso mano sx  -->   RX.Mano.Polso.Sx 
    ''' 
    ''' 
    ''' </summary>
    Private Function ParseRowView(rowView As RowView) As RowView
        If rowView.ItemRow.State = ParsingStates.ManuallyDesigned Then Return rowView
        Dim cell = rowView.ItemRow.Cells.Single(Function(c) c.FieldName = FieldToBeParsed.Name)
        cell.Matches = New List(Of ItemRow.ClauseMatch)
        cell.UnparsedText = cell.Text
        For Each clause In _project.Clauses
            Dim ctext = ExcludeText(cell.Text)
            Dim matchedVocs = New List(Of Vocabulary)
            Dim clauseMatch As ItemRow.ClauseMatch = Nothing
            For Each voc In clause.SortedVocs.Where(Function(v) Not String.IsNullOrEmpty(v.Pattern))
                For Each m As Match In Regex.Matches(ctext, voc.Pattern, RegexOptions.IgnoreCase)
                    If Not String.IsNullOrEmpty(m.Groups(voc.Name).Value) Then 'AndAlso Not matchedVocs.Contains(voc) Then
                        matchedVocs.Add(voc)
                        If clauseMatch Is Nothing Then clauseMatch = New ItemRow.ClauseMatch
                        clauseMatch.Clause = clause
                        Dim word = m.Groups(voc.Name).Value
                        ctext = ctext.Replace(word, "")
                        If voc.Synonyms.Any Then
                            Dim w = voc.Synonyms.SingleOrDefault(Function(s) s.Terms.Any(Function(t) t.ToLower = word.ToLower))
                            If w IsNot Nothing Then word = w.MainTerm
                        End If
                        clauseMatch.Words.Add(New ItemRow.WordMatch(word, voc.Name, m.Groups(voc.Name).Index))
                    End If
                Next

            Next
            If clauseMatch IsNot Nothing Then
                clauseMatch.UnmatchedText = ctext
                cell.Matches.Add(clauseMatch)
            End If

        Next
        If cell.Matches.Any Then
            cell.WinnerClause = cell.Matches.OrderBy(Function(m) m.Clause.SortedVocs.Count).Last
        End If
        SetParsedCell(cell, rowView)
        Return rowView
    End Function

    Private Function ExcludeText(text As String) As String
        text = Regex.Replace(text, " +", " ")
        For Each voc In _project.Vocabularies.Where(Function(v) v.Name.Substring(0, 1) = "$")
            text = Regex.Replace(text, voc.Pattern, "", RegexOptions.IgnoreCase)
        Next
        Return text
    End Function

    Private Function GetFormatLines() As IEnumerable(Of String)
        Dim lines = New List(Of String)
        For Each m As Match In Regex.Matches(_project.Formats, "[^\n\r]+")
            lines.Add(m.Value)
        Next
        Return lines
    End Function

    Private Function GetVocabularySequence(formatLine As String) As IEnumerable(Of Vocabulary)
        Dim vocs = New List(Of Vocabulary)
        For Each vocName In formatLine.Split(".")
            vocs.Add(_project.Vocabularies.SingleOrDefault(Function(v) v.Name.ToLower = vocName.ToLower))
        Next
        Return vocs.Except({Nothing})
    End Function

    Private Sub SetParsedCell(cell As ItemRow.Cell, rowView As RowView)
        If Not cell.Matches.Any Then
            rowView.GridRow.Cells(_specialColumnIndex(Columns.Parsed)).Value = ""
            rowView.GridRow.Cells(_specialColumnIndex(Columns.UnMatched)).Value = ""
            Exit Sub
        End If
        SortMatchedWords(cell)
        Dim sb = New StringBuilder

        cell.WinnerClause = cell.Matches.OrderBy(Function(m) m.Words.Count).Last
        'For Each w In cell.Matches.OrderBy(Function(m) m.Words.Count).Last.Words  'the match with most vocs
        For Each w In cell.WinnerClause.Words
            sb.Append(If(String.IsNullOrEmpty(sb.ToString), w.Value, $".{w.Value}"))
        Next
        cell.ParsedText = sb.ToString
        If Not String.IsNullOrEmpty(cell.WinnerClause.Clause.Prefix) Then cell.ParsedText = $"{cell.WinnerClause.Clause.Prefix}.{cell.ParsedText}"
        Dim grdCellParsed = rowView.GridRow.Cells(_specialColumnIndex(Columns.Parsed))
        grdCellParsed.Tag = cell
        grdCellParsed.Value = cell.ParsedText
        rowView.ItemRow.ParseResult = cell.ParsedText

        rowView.ItemRow.UnparsedText = cell.WinnerClause.UnmatchedText
        rowView.GridRow.Cells(_specialColumnIndex(Columns.UnMatched)).Value = cell.WinnerClause.UnmatchedText

        rowView.ItemRow.Cells(_specialColumnIndex(Columns.Parsed)).Text = cell.ParsedText
        rowView.ItemRow.Cells(_specialColumnIndex(Columns.UnMatched)).Text = cell.UnparsedText

    End Sub

    Private Sub SortMatchedWords(cell As ItemRow.Cell)
        Dim words = New List(Of ItemRow.WordMatch)
        Dim unsortedWords = New List(Of ItemRow.WordMatch)
        unsortedWords.AddRange(cell.WinnerClause.Words)
        For Each voc In cell.WinnerClause.Clause.SortedVocs
            Dim word = unsortedWords.FirstOrDefault(Function(w) w.VocName = voc.Name)
            If word IsNot Nothing Then
                words.Add(word)
                unsortedWords.Remove(word)
            End If
        Next
        cell.WinnerClause.Words.Clear()
        cell.WinnerClause.Words.AddRange(words)
    End Sub

    Private Sub SetTooltip(gridCell As DataGridViewCell)
        Dim cell = DirectCast(gridCell.Tag, ItemRow.Cell)
        gridCell.ToolTipText = ""
        If cell Is Nothing OrElse cell.Matches Is Nothing Then Exit Sub
        Dim sb = New StringBuilder
        For Each w In cell.WinnerClause.Words
            sb.AppendLine($"{w.VocName}: {w.Value}")
        Next
        sb.AppendLine("")
        sb.AppendLine($"Winner Format: {cell.WinnerClause.Clause.Text}")
        If cell.Matches.Count > 1 Then
            sb.AppendLine($"Other Formats:")
            For i = 1 To cell.Matches.Count - 1
                sb.AppendLine($"  {cell.Matches(i).Clause.Text}")
            Next
        End If

        Dim itemRow = _rowViews.Single(Function(rv) rv.GridRow.Index = gridCell.RowIndex).ItemRow
        If itemRow.HidingParseResults.Any Then
            sb.AppendLine($"")
            sb.AppendLine($"Hiding:")
            For Each hiding In itemRow.HidingParseResults
                sb.AppendLine(hiding)
            Next
        End If
        gridCell.ToolTipText = sb.ToString
    End Sub

    Private Sub GrdItems_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles GrdItems.CellMouseDown
        If e.ColumnIndex = -1 Then Exit Sub

        If e.Button = MouseButtons.Right Then
            If e.RowIndex > -1 AndAlso e.ColumnIndex > -1 Then
                GrdItems.CurrentCell = GrdItems(e.ColumnIndex, e.RowIndex)
            Else
                GrdItems.CurrentCell = Nothing
            End If
            MenuCell(GrdItems.Columns(e.ColumnIndex).HeaderText, GrdItems.CurrentCell)

        Else
            If ModifierKeys.HasFlag(Keys.Shift) Then
                SetTooltip(GrdItems(e.ColumnIndex, e.RowIndex))
            End If
        End If
    End Sub

    Private Sub CellText_MouseDown(sender As Object, e As MouseEventArgs) Handles _cellText.MouseDown
        If e.Button = MouseButtons.Right Then
            Dim cell = DirectCast(_cellText.Tag, DataGridViewCell)
            MenuCell(cell.OwningColumn.HeaderText, cell, _cellText.SelectedText)
        End If
    End Sub

    Private Sub GrdItems_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles GrdItems.CellBeginEdit
        'e.Cancel = e.RowIndex <> 0
    End Sub

    Private Sub Search(searchText As String, fieldName As String)
        Static lastSearchText As String = ""
        If searchText = lastSearchText Then Exit Sub
        lastSearchText = searchText
        If String.IsNullOrEmpty(searchText?.Trim) Then
            GrdItems.EndEdit()
            BeginInvoke(Sub()
                            ShowItemRows(_allRowViews.Select(Function(r) r.ItemRow))
                        End Sub)
        Else
            searchText = searchText.ToLower
            _rowViews = _rowViews.Where(Function(r) r.ItemRow.Cells.Single(Function(c) c.FieldName = fieldName).Text.ToLower.Contains(searchText)).ToList
            ShowItemRows(_rowViews.Select(Function(dr) dr.ItemRow))
        End If
    End Sub

    Private Sub GrdItems_MouseWheel(sender As Object, e As MouseEventArgs) Handles GrdItems.MouseWheel
        GrdItems.StepFont(e.Delta)
    End Sub


    Private Sub GrdItems_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles GrdItems.EditingControlShowing
        If GrdItems.CurrentCell.RowIndex = 0 Then
            _cellSearch = DirectCast(e.Control, TextBox)
            _cellSearch.Tag = GrdItems.CurrentCell.OwningColumn.HeaderText
        Else
            _cellText = DirectCast(e.Control, TextBox)
            _cellText.Tag = GrdItems.CurrentCell
            _cellText.ContextMenuStrip = New ContextMenuStrip
            If GrdItems.CurrentCell.ColumnIndex = _specialColumnIndex(Columns.Parsed) Then
                If _intellisense Is Nothing Then
                    Dim words = GrdItems.CurrentRow.Cells(_specialColumnIndex(Columns.UnMatched)).Value?.ToString.GetWords
                    _intellisense = New Intellisense(_cellText, words)
                Else
                    _intellisense.Words = GrdItems.CurrentRow.Cells(_specialColumnIndex(Columns.UnMatched)).Value?.ToString.GetWords
                End If
            Else
                _intellisense = Nothing
            End If
        End If
    End Sub

    Private Sub VocabularyEditor_VoceNameCHanged(oldLabel As String, newLabel As String) Handles VocabularyEditor._VoceNameCHanged
        FormatEditor.Update(oldLabel, newLabel)
    End Sub

    Private Sub _cellSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles _cellSearch.KeyDown
        If e.KeyCode = Keys.Return OrElse e.KeyCode = Keys.F1 Then
            e.SuppressKeyPress = True
            Search(_cellSearch.Text, _cellSearch.Tag.ToString)
        End If
    End Sub

    Private Sub BuildToolstripFields()
        ToolStripGrid.Items.Clear()
        ToolStripGrid.ForeColor = Color.Black
        ToolStripGrid.Items.Add("Clear", My.Resources.clearall, Sub()
                                                                    '_fieldToBeParsed = Nothing
                                                                    '_project.Nomenclature.Fields.Clear()
                                                                    _project.Nomenclature.Rows.Clear()
                                                                    GrdItems.Rows.Clear()
                                                                End Sub)
        MenuFieldButtons()

        ToolStripGrid.Items.Add("-")

        MenuCounterButtons()
    End Sub

    Private Sub MenuCounterButtons()
        For Each state As ParsingStates In System.Enum.GetValues(GetType(ParsingStates))
            'If state <> ParsingStates.Undefined Then
            Dim tState = state
            Dim mButt = New ToolStripButton(tState.ToString)
            mButt.Alignment = ToolStripItemAlignment.Right
            _buttonCounters(tState) = mButt
            mButt.Tag = tState
            mButt.CheckOnClick = True

            Select Case state
                '''
                Case ParsingStates.Unique
                    mButt.ForeColor = _colors.Unique
                Case ParsingStates.Ambiguous
                    mButt.ForeColor = _colors.Ambiguous
                Case ParsingStates.UnMatched
                    mButt.ForeColor = _colors.UnMatched
                Case ParsingStates.Excluded
                    mButt.ForeColor = _colors.Excluded
                Case ParsingStates.Underspecified
                    mButt.ForeColor = _colors.Underspecified

            End Select
            AddHandler mButt.CheckedChanged,
                Sub()
                    Dim filteredRows = _allRowViews.Select(Function(rv) rv.ItemRow)
                    If tState <> ParsingStates.All Then
                        filteredRows = _allRowViews.Where(Function(rv) rv.ItemRow.State = tState).Select(Function(r) r.ItemRow)
                    End If
                    ShowItemRows(filteredRows)
                End Sub
            ToolStripGrid.Items.Add(mButt)
            'End If
        Next
        _selectedItemsLabel = New ToolStripLabel("")
        _selectedItemsLabel.Font = New Font(_selectedItemsLabel.Font.FontFamily, _selectedItemsLabel.Font.Size, FontStyle.Bold)
        _selectedItemsLabel.BackColor = Color.Yellow
        _selectedItemsLabel.ForeColor = _colors.Counter

        _selectedItemsLabel.Alignment = ToolStripItemAlignment.Right
        ToolStripGrid.Items.Add(_selectedItemsLabel)

    End Sub

    Private Sub MenuFieldButtons()
        For Each field In _project.Nomenclature.Fields
            Dim col = GrdItems.GetColFromFieldName(field.Name)
            Dim mButt = New ToolStripButton(col.HeaderText, Nothing)
            If Not _buttonsDict.ContainsKey(col.HeaderText) Then _buttonsDict.Add(field.Name, mButt)
            mButt.CheckOnClick = True
            AddHandler mButt.CheckedChanged, Sub()
                                                 col.Visible = mButt.Checked
                                             End Sub
            mButt.Checked = col.Visible
            ToolStripGrid.Items.Add(mButt)
        Next

        MenuFieldToParse()

        _parseButton = New ToolStripMenuItem("Parse ->", Nothing, Sub() ParseAll())
        _parseButton.Visible = False
        ToolStripGrid.Items.Insert(0, _parseButton)
        ToolStripGrid.Items.Insert(1, _fieldToBeParsedButton)

        ToolStripGrid.Items.Insert(2, New ToolStripSeparator)
    End Sub

    Private Sub MenuFieldToParse()
        _fieldToBeParsedButton = New ToolStripMenuItem("Parse What?", Nothing)
        For Each field In _project.Nomenclature.Fields
            Dim tField = field
            _fieldToBeParsedButton.DropDownItems.Add(tField.Name, Nothing,
                                             Sub()
                                                 SetParseField(tField)
                                             End Sub)
            _fieldToBeParsedButton.ForeColor = Color.Red
        Next
    End Sub

    Private Sub SetParseField(field As Nomenclature.Field)
        If field Is Nothing Then Exit Sub
        _fieldToBeParsedButton.Text = field.Name
        _fieldToBeParsedButton.ForeColor = Color.DarkOliveGreen ' _colors.Parsed
        FieldToBeParsed = field
        _parseButton.Visible = True
        _project.ParsedField = field
        'OutputTable.ItemField = _fieldToBeParsed
        For Each fld In _project.Nomenclature.Fields.Except({FieldToBeParsed})
            GrdItems.GetColFromFieldName(fld.Name).Visible = False
            _buttonsDict(fld.Name).Checked = False
        Next
    End Sub

    Private Sub GrdItems_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles GrdItems.CellValueChanged

    End Sub

    Private Sub GrdItems_CellStateChanged(sender As Object, e As DataGridViewCellStateChangedEventArgs) Handles GrdItems.CellStateChanged
        'If e.Cell.ColumnIndex = _specialColumnIndex(Columns.Approved) Then
        'If CBool(e.Cell.Value) Then
        '    GrdItems(_specialColumnIndex(Columns.Parsed), e.Cell.RowIndex).Style.BackColor = Color.Green
        'Else
        '    GrdItems(_specialColumnIndex(Columns.Parsed), e.Cell.RowIndex).Style.BackColor = Color.Black
        'End If
        'End If
    End Sub


    Private Sub GrdItems_ColumnHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles GrdItems.ColumnHeaderMouseClick
        Sort(e.ColumnIndex)
    End Sub

    Private Sub Sort(colIndex As Integer)
        GrdItems.Enabled = False
        _sortedDescending = Not _sortedDescending
        If FieldToBeParsed Is Nothing Then
            MessageBox.Show("Specify the parsed field before sorting")
            Exit Sub
        End If
        Dim icellIndex = GetColFromFieldName(GrdItems, FieldToBeParsed.Name).Index
        If _sortedDescending Then
            Select Case colIndex
                Case _specialColumnIndex(Columns.Parsed)
                    _rowViews = _rowViews.OrderByDescending(Function(r) r.ItemRow.Cells(icellIndex).ParsedText).ToList

                Case _specialColumnIndex(Columns.UnMatched)
                    _rowViews = _rowViews.OrderByDescending(Function(r) r.ItemRow.Cells(icellIndex).UnparsedText).ToList

                Case Else
                    _rowViews = _rowViews.OrderByDescending(Function(r) r.GridRow.Cells(colIndex).Value?.ToString).ToList
            End Select
        Else
            Select Case colIndex
                Case _specialColumnIndex(Columns.Parsed)
                    _rowViews = _rowViews.OrderBy(Function(r) r.ItemRow.Cells(icellIndex).ParsedText).ToList

                Case _specialColumnIndex(Columns.UnMatched)
                    _rowViews = _rowViews.OrderBy(Function(r) r.ItemRow.Cells(icellIndex).UnparsedText).ToList

                Case Else
                    _rowViews = _rowViews.OrderByDescending(Function(r) r.GridRow.Cells(colIndex).Value?.ToString).ToList
            End Select

        End If
        ShowItemRows(_rowViews.Select(Function(r) r.ItemRow))
        GrdItems.Enabled = True
    End Sub

    Private Sub GrdItems_CellMouseEnter(sender As Object, e As DataGridViewCellEventArgs) Handles GrdItems.CellMouseEnter
        If e.ColumnIndex = _specialColumnIndex(Columns.Parsed) AndAlso e.RowIndex > -1 Then SetTooltip(GrdItems(e.ColumnIndex, e.RowIndex))
    End Sub

    Private Sub GrdItems_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles GrdItems.CellContentClick
        If e.ColumnIndex = _specialColumnIndex(Columns.OK) Then
            Dim cell = DirectCast(GrdItems.Rows(e.RowIndex).Cells(_specialColumnIndex(Columns.OK)), DataGridViewCheckBoxCell)
            cell.Value = Not CBool(cell.Value)
            If CBool(cell.Value) Then
                GrdItems(_specialColumnIndex(Columns.Parsed), e.RowIndex).Style.ForeColor = _colors.Ok
            Else
                GrdItems(_specialColumnIndex(Columns.Parsed), e.RowIndex).Style.ForeColor = _colors.Parsed
            End If
        End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub GrdItems_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles GrdItems.DataError
        If {Columns.OK, Columns.Excluded}.Contains(0) Then  'resolve the problem of boolean columns
        End If
    End Sub
End Class
