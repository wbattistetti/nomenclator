﻿Imports System.Text.RegularExpressions

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GrdItems = New System.Windows.Forms.DataGridView()
        Me.ToolStrip = New System.Windows.Forms.ToolStrip()
        Me.SplitTabs = New System.Windows.Forms.SplitContainer()
        Me.ToolStripGrid = New System.Windows.Forms.ToolStrip()
        Me.VocabularyEditor = New Nomenclator.VocabularyEditor()
        Me.SplitMain = New System.Windows.Forms.SplitContainer()
        Me.FormatEditor = New Nomenclator.FormatEditor()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.TabControl = New System.Windows.Forms.TabControl()
        Me.TabItems = New System.Windows.Forms.TabPage()
        Me.TabPrompts = New System.Windows.Forms.TabPage()
        Me.OutputTable = New Nomenclator.OutputTable()
        CType(Me.GrdItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitTabs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitTabs.Panel1.SuspendLayout()
        Me.SplitTabs.Panel2.SuspendLayout()
        Me.SplitTabs.SuspendLayout()
        CType(Me.SplitMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitMain.Panel1.SuspendLayout()
        Me.SplitMain.Panel2.SuspendLayout()
        Me.SplitMain.SuspendLayout()
        Me.TabControl.SuspendLayout()
        Me.TabItems.SuspendLayout()
        Me.TabPrompts.SuspendLayout()
        Me.SuspendLayout()
        '
        'GrdItems
        '
        Me.GrdItems.AllowUserToAddRows = False
        Me.GrdItems.BackgroundColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GrdItems.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.GrdItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GrdItems.DefaultCellStyle = DataGridViewCellStyle2
        Me.GrdItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GrdItems.Location = New System.Drawing.Point(0, 25)
        Me.GrdItems.Name = "GrdItems"
        Me.GrdItems.RowTemplate.Height = 25
        Me.GrdItems.Size = New System.Drawing.Size(783, 405)
        Me.GrdItems.TabIndex = 0
        '
        'ToolStrip
        '
        Me.ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip.Name = "ToolStrip"
        Me.ToolStrip.Size = New System.Drawing.Size(1248, 25)
        Me.ToolStrip.TabIndex = 1
        Me.ToolStrip.Text = "ToolStrip1"
        '
        'SplitTabs
        '
        Me.SplitTabs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitTabs.Cursor = System.Windows.Forms.Cursors.VSplit
        Me.SplitTabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitTabs.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitTabs.Location = New System.Drawing.Point(3, 3)
        Me.SplitTabs.Name = "SplitTabs"
        '
        'SplitTabs.Panel1
        '
        Me.SplitTabs.Panel1.Controls.Add(Me.GrdItems)
        Me.SplitTabs.Panel1.Controls.Add(Me.ToolStripGrid)
        '
        'SplitTabs.Panel2
        '
        Me.SplitTabs.Panel2.Controls.Add(Me.VocabularyEditor)
        Me.SplitTabs.Size = New System.Drawing.Size(1234, 432)
        Me.SplitTabs.SplitterDistance = 785
        Me.SplitTabs.TabIndex = 1
        '
        'ToolStripGrid
        '
        Me.ToolStripGrid.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStripGrid.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripGrid.Name = "ToolStripGrid"
        Me.ToolStripGrid.Size = New System.Drawing.Size(783, 25)
        Me.ToolStripGrid.TabIndex = 1
        Me.ToolStripGrid.Text = "ToolStrip1"
        '
        'VocabularyEditor
        '
        Me.VocabularyEditor.BackColor = System.Drawing.Color.Black
        Me.VocabularyEditor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.VocabularyEditor.Location = New System.Drawing.Point(0, 0)
        Me.VocabularyEditor.Name = "VocabularyEditor"
        Me.VocabularyEditor.Size = New System.Drawing.Size(443, 430)
        Me.VocabularyEditor.TabIndex = 5
        Me.VocabularyEditor.Vocabularies = Nothing
        '
        'SplitMain
        '
        Me.SplitMain.Cursor = System.Windows.Forms.Cursors.HSplit
        Me.SplitMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitMain.Location = New System.Drawing.Point(0, 25)
        Me.SplitMain.Name = "SplitMain"
        Me.SplitMain.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitMain.Panel1
        '
        Me.SplitMain.Panel1.Controls.Add(Me.FormatEditor)
        Me.SplitMain.Panel1.Controls.Add(Me.lbl1)
        '
        'SplitMain.Panel2
        '
        Me.SplitMain.Panel2.Controls.Add(Me.TabControl)
        Me.SplitMain.Size = New System.Drawing.Size(1248, 613)
        Me.SplitMain.SplitterDistance = 143
        Me.SplitMain.TabIndex = 6
        '
        'FormatEditor
        '
        Me.FormatEditor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FormatEditor.Location = New System.Drawing.Point(0, 23)
        Me.FormatEditor.Name = "FormatEditor"
        Me.FormatEditor.Project = Nothing
        Me.FormatEditor.Size = New System.Drawing.Size(1248, 120)
        Me.FormatEditor.TabIndex = 7
        '
        'lbl1
        '
        Me.lbl1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lbl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.lbl1.Location = New System.Drawing.Point(0, 0)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(1248, 23)
        Me.lbl1.TabIndex = 6
        Me.lbl1.Text = "Destination Format"
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TabControl
        '
        Me.TabControl.Controls.Add(Me.TabItems)
        Me.TabControl.Controls.Add(Me.TabPrompts)
        Me.TabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl.Location = New System.Drawing.Point(0, 0)
        Me.TabControl.Name = "TabControl"
        Me.TabControl.SelectedIndex = 0
        Me.TabControl.Size = New System.Drawing.Size(1248, 466)
        Me.TabControl.TabIndex = 2
        '
        'TabItems
        '
        Me.TabItems.Controls.Add(Me.SplitTabs)
        Me.TabItems.Location = New System.Drawing.Point(4, 24)
        Me.TabItems.Name = "TabItems"
        Me.TabItems.Padding = New System.Windows.Forms.Padding(3)
        Me.TabItems.Size = New System.Drawing.Size(1240, 438)
        Me.TabItems.TabIndex = 0
        Me.TabItems.Text = "Items"
        Me.TabItems.UseVisualStyleBackColor = True
        '
        'TabPrompts
        '
        Me.TabPrompts.Controls.Add(Me.OutputTable)
        Me.TabPrompts.Location = New System.Drawing.Point(4, 24)
        Me.TabPrompts.Name = "TabPrompts"
        Me.TabPrompts.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPrompts.Size = New System.Drawing.Size(1242, 440)
        Me.TabPrompts.TabIndex = 1
        Me.TabPrompts.Text = "Prompts"
        Me.TabPrompts.UseVisualStyleBackColor = True
        '
        'OutputTable
        '
        Me.OutputTable.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OutputTable.Location = New System.Drawing.Point(3, 3)
        Me.OutputTable.Name = "OutputTable"
        Me.OutputTable.Size = New System.Drawing.Size(1236, 434)
        Me.OutputTable.TabIndex = 0
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.ClientSize = New System.Drawing.Size(1248, 638)
        Me.Controls.Add(Me.SplitMain)
        Me.Controls.Add(Me.ToolStrip)
        Me.ForeColor = System.Drawing.Color.White
        Me.Name = "FrmMain"
        Me.Text = "NOMENCLATOR"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GrdItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitTabs.Panel1.ResumeLayout(False)
        Me.SplitTabs.Panel1.PerformLayout()
        Me.SplitTabs.Panel2.ResumeLayout(False)
        CType(Me.SplitTabs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitTabs.ResumeLayout(False)
        Me.SplitMain.Panel1.ResumeLayout(False)
        Me.SplitMain.Panel2.ResumeLayout(False)
        CType(Me.SplitMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitMain.ResumeLayout(False)
        Me.TabControl.ResumeLayout(False)
        Me.TabItems.ResumeLayout(False)
        Me.TabPrompts.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private Sub GrdItems_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles GrdItems.CellEndEdit
        If GrdItems.Rows(e.RowIndex).Frozen Then
            Dim cell = GrdItems(e.ColumnIndex, e.RowIndex)
            Search(cell.Value?.ToString, cell.OwningColumn.HeaderText)
        Else
            Select Case e.ColumnIndex
                Case _specialColumnIndex(Columns.Parsed)
                    SeParsingManually(e)
            End Select
        End If
    End Sub

    Private Sub SeParsingManually(e As DataGridViewCellEventArgs)
        GrdItems(e.ColumnIndex, e.RowIndex).Style.BackColor = Color.Green
        Dim rv = _rowViews.Single(Function(r) r.GridRow Is GrdItems.Rows(e.RowIndex))
        Dim cellText = GrdItems(e.ColumnIndex, e.RowIndex).Value.ToString
        rv.ItemRow.State = ParsingStates.ManuallyDesigned
        rv.ItemRow.ParseResult = cellText
        Dim parsedWords = cellText?.ToString.GetWords
        
        cellText = GrdItems(_specialColumnIndex(Columns.UnMatched), e.RowIndex).Value?.ToString
        If cellText IsNot Nothing Then
            For Each w In parsedWords
                cellText = Regex.Replace(cellText, $"\b{w}\b", "", RegexOptions.IgnoreCase)
            Next
        End If
        GrdItems(_specialColumnIndex(Columns.UnMatched), e.RowIndex).Value = cellText
    End Sub

    Friend WithEvents GrdItems As DataGridView
    Friend WithEvents ToolStrip As ToolStrip
    Friend WithEvents SplitTabs As SplitContainer
    Friend WithEvents SplitMain As SplitContainer
    Friend WithEvents lbl1 As Label
    Friend WithEvents FormatEditor As FormatEditor
    Friend WithEvents VocabularyEditor As VocabularyEditor
    Friend WithEvents ToolStripGrid As ToolStrip
    Friend WithEvents TabControl As TabControl
    Friend WithEvents TabItems As TabPage
    Friend WithEvents TabPrompts As TabPage
    Friend WithEvents OutputTable As Nomenclator.OutputTable
End Class
