﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormatEditor
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.RTFormats = New System.Windows.Forms.RichTextBox()
        Me.SuspendLayout()
        '
        'RTFormats
        '
        Me.RTFormats.BackColor = System.Drawing.Color.Black
        Me.RTFormats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RTFormats.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RTFormats.ForeColor = System.Drawing.Color.White
        Me.RTFormats.Location = New System.Drawing.Point(0, 0)
        Me.RTFormats.Name = "RTFormats"
        Me.RTFormats.Size = New System.Drawing.Size(1110, 303)
        Me.RTFormats.TabIndex = 2
        Me.RTFormats.Text = ""
        '
        'FormatEditor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.RTFormats)
        Me.Name = "FormatEditor"
        Me.Size = New System.Drawing.Size(1110, 303)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RTFormats As RichTextBox
End Class
