﻿Imports System.ComponentModel
Imports System.Text
Imports System.Text.RegularExpressions

Public Class VocabularyEditor
    Public Event _VoceNameChanged(oldLabel As String, newLabel As String)

    Private Structure Patterns
        Public Rendering As String
        Public Synonyms As String
    End Structure

    Private Enum PatternGroups
        Filler
        Term
        Colloquial
        Synonym
    End Enum

    Private _selectedVoc As Vocabulary = Nothing
    Private _vocabularies As BindingList(Of Vocabulary)

    Private _displayState As DisplayStates
    Private _ignore As Boolean
    Private _patterns As Patterns

    Private Property DisplayState As DisplayStates
        Get
            Return _displayState
        End Get
        Set(value As DisplayStates)
            UpdateVocabulary()
            _displayState = value
            ShowVocabulary(_selectedVoc)
        End Set
    End Property

    Private Enum DisplayStates
        Words
        Synonyms
        Renderings
    End Enum

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        SplitWords.Panel2Collapsed = True
        SetPatterns()
        DisplayState = DisplayStates.Words
        BuildToolstrip()
        ShowCheck(False)
    End Sub

    Private Sub SetPatterns()
        _patterns.Rendering = $"^((?<{PatternGroups.Filler}>[^-]+)-)?(?<{PatternGroups.Term}>[^\r\t:]+)(:(?<{PatternGroups.Colloquial}>[^\r\t.]+))?"
        _patterns.Synonyms = $"[\r\t]?(<{PatternGroups.Term.ToString}>[^:]+) *: *(<{PatternGroups.Synonym.ToString}>[^\r\t.]+)"
    End Sub

    Public Property Vocabularies As BindingList(Of Vocabulary)
        Get
            Return _vocabularies
        End Get
        Set(value As BindingList(Of Vocabulary))
            _vocabularies = value
            GrdVocabularies.Rows.Clear()
            If value Is Nothing Then Exit Property
            For Each voc In _vocabularies.OrderBy(Function(v) v.Name)
                Dim row = New DataGridViewRow
                row.Tag = voc
                row.CreateCells(GrdVocabularies)
                row.Cells(ColIcon.Index).Value = My.Resources.Vocabulary
                row.Cells(ColName.Index).Value = voc.Name
                row.Cells(ColCount.Index).Value = voc.Words.Count
                row.Tag = voc
                GrdVocabularies.Rows.Add(row)
            Next
        End Set
    End Property

    Public WriteOnly Property ClipboardBox As String
        Set(value As String)
            RTClipBoard1.Text = value
            SplitWords.Panel2Collapsed = String.IsNullOrEmpty(value.Trim)
        End Set
    End Property

    Public Sub AddWord(voc As Vocabulary, word As String)
        voc.Words.Add(word)
        SelectVoc(voc)
    End Sub

    Public Sub CompileVocabularies()
        UpdateVocabulary()
        For Each voc In _vocabularies
            Dim words = voc.Words.Concat(voc.Synonyms.Select(Function(s) s.MainTerm)).
                                         Concat(voc.Synonyms.SelectMany(Function(s) s.Terms))
            If words.Any Then
                Dim sbWords = New StringBuilder
                Dim i = 0
                For Each word In words.OrderByDescending(Function(w) w.Length)
                    sbWords.Append(If(i = 0, $"\b({word})\b", $"|\b({word})\b"))
                    i += 1
                Next
                voc.Pattern = $"(?<{voc.Name.Replace("$", "")}>{sbWords.ToString})"
                GetRowFromVocName(voc).Cells(ColIcon.Index).Value = My.Resources.Vocabulary
            Else
                voc.Pattern = ""
                GetRowFromVocName(voc).Cells(ColIcon.Index).Value = My.Resources.VocabularyEmpty
            End If
        Next
    End Sub

    Private Function GetRowFromVocName(voc As Vocabulary) As DataGridViewRow
        For Each row As DataGridViewRow In GrdVocabularies.Rows
            If row.Tag Is voc Then
                Return row
                Exit For
            End If
        Next
        Return Nothing
    End Function

    Public Sub Clear()
        GrdVocabularies.Rows.Clear()
        RtBox.Clear()
    End Sub

    Private Sub TxtAdd_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.Return Then

        End If
    End Sub

    Private Sub AddVocabulary(name As String)
        name = name.Trim
        If Not _vocabularies.Any(Function(v) v.Name.ToLower = name.ToLower) Then
            Dim row = New DataGridViewRow
            row.CreateCells(GrdVocabularies)
            Dim newVoc = New Vocabulary(name)
            row.Cells(ColName.Index).Value = newVoc.Name
            row.Tag = _selectedVoc
            _vocabularies.Add(newVoc)
            GrdVocabularies.ClearSelection()
            RtBox.Text = ""
            Vocabularies = _vocabularies 'force refresh
            _selectedVoc = newVoc
            BeginInvoke(Sub() SelectVoc(_selectedVoc))
        Else

        End If
    End Sub

    Private Sub SelectVoc(voc As Vocabulary)
        GrdVocabularies.ClearSelection()
        For Each row As DataGridViewRow In GrdVocabularies.Rows
            If row.Tag Is voc Then
                row.Selected = True
                _selectedVoc = voc
                ShowVocabulary(voc)
                Exit For
            End If
        Next
    End Sub

    Private Sub GrdVocabularies_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles GrdVocabularies.RowEnter
        If _selectedVoc IsNot Nothing Then UpdateVocabulary()

        If e.RowIndex = -1 Then
            _selectedVoc = Nothing
        Else
            _selectedVoc = DirectCast(GrdVocabularies.Rows(e.RowIndex).Tag, Vocabulary)
        End If
        ShowVocabulary(_selectedVoc)
    End Sub

    Private Sub ShowVocabulary(voc As Vocabulary)
        If voc Is Nothing Then
            RtBox.Text = ""
            RtBox.Enabled = False
        Else
            Select Case DisplayState
                Case DisplayStates.Words
                    ShowWords(voc)
                Case DisplayStates.Synonyms
                    ShowSynonyms(voc)
                Case DisplayStates.Renderings
                    ShowRendering(voc)
            End Select
        End If
        FormatText(voc)
    End Sub

    Private Sub ShowWords(voc As Vocabulary)
        RtBox.Clear()
        For Each st In voc.Words.OrderBy(Function(w) w)
            RtBox.AppendText($"{st}{Environment.NewLine}")
        Next
        RtBox.Enabled = True
    End Sub

    Private Sub ShowSynonyms(voc As Vocabulary)
        RtBox.Clear()
        For Each synonym In voc.Synonyms.OrderBy(Function(s) s.MainTerm)
            Dim sbTerms = New StringBuilder
            Dim i = 0
            For Each term In synonym.Terms.OrderBy(Function(w) w)
                Dim st = If(i = 0, term, $", {term}")
                sbTerms.AppendLine(st.Trim)
                i += 1
            Next
            RtBox.AppendText($"{synonym.MainTerm}: {sbTerms.ToString}{Environment.NewLine}")
        Next
        RtBox.Enabled = True
    End Sub

    Private Sub ShowRendering(voc As Vocabulary)
        RtBox.Clear()
        For Each rendering In voc.Renderings.OrderBy(Function(w) w.Term)
            Dim st = rendering.Term
            If Not String.IsNullOrEmpty(rendering.Filler) Then
                st = $"{rendering.Filler}-{rendering.Term}"
            End If
            If Not String.IsNullOrEmpty(rendering.ColloquialTerm) Then
                    st = $"{st}: {rendering.ColloquialTerm}"
                End If
                RtBox.AppendText($"{st}{Environment.NewLine}")
        Next
        RtBox.Enabled = True
    End Sub

    Private Sub FormatText(voc As Vocabulary)
        If _ignore OrElse _selectedVoc Is Nothing Then Exit Sub
        _ignore = True
        With RtBox
            .SuspendLayout()
            Dim s = .SelectionStart
            .SelectionStart = 0
            .SelectionLength = .Text.Length
            .SelectionColor = Color.White

            Select Case DisplayState
                Case DisplayStates.Synonyms
                    For Each mline As Match In Regex.Matches(.Text, "[^\r\n]+")
                        For Each m As Match In Regex.Matches(mline.Value, _patterns.Synonyms)
                            .SelectionStart = mline.Index + m.Groups(PatternGroups.Term.ToString).Index
                            .SelectionLength = m.Groups(PatternGroups.Term.ToString).Length
                            .SelectionColor = Color.White  'the main term color
                            For Each m1 As Match In Regex.Matches(m.Groups(PatternGroups.Synonym.ToString).Value, $"([^,]+)")
                                .SelectionStart = mline.Index + m.Groups(PatternGroups.Term.ToString).Index + m.Groups(PatternGroups.Synonym.ToString).Index
                                .SelectionLength = m.Groups(PatternGroups.Synonym.ToString).Length
                                .SelectionColor = Color.LightGreen 'the synoyms color
                            Next
                        Next
                    Next

                Case DisplayStates.Renderings
                    For Each mline As Match In Regex.Matches(.Text, "[^\r\n]+")
                        For Each m As Match In Regex.Matches(mline.Value, _patterns.Rendering)
                            .SelectionStart = mline.Index + m.Groups(PatternGroups.Filler.ToString).Index
                            .SelectionLength = m.Groups(PatternGroups.Filler.ToString).Length
                            .SelectionColor = Color.Yellow   'the filler term color

                            .SelectionStart = mline.Index + m.Groups(PatternGroups.Term.ToString).Index
                            .SelectionLength = m.Groups(PatternGroups.Term.ToString).Length
                            .SelectionColor = Color.White 'the term color

                            .SelectionStart = mline.Index + m.Groups(PatternGroups.Colloquial.ToString).Index
                            .SelectionLength = m.Groups(PatternGroups.Colloquial.ToString).Length
                            .SelectionColor = Color.Orange 'the colloquial color

                        Next
                    Next
            End Select

            ResumeLayout()
            .SelectionStart = s
            .SelectionLength = 0
            .SelectionColor = Color.White
        End With
        _ignore = False
    End Sub

    Public Sub UpdateVocabulary()
        Select Case DisplayState
            Case DisplayStates.Words
                UpdateVocabularyWords(_selectedVoc)
            Case DisplayStates.Synonyms
                UpdateVocabularySynonyms(_selectedVoc)
            Case DisplayStates.Renderings
                UpdateVocabularyRendering(_selectedVoc)
        End Select
    End Sub

    Public Sub UpdateVocabularyWords(voc As Vocabulary)
        If voc Is Nothing Then Exit Sub
        voc.Words.Clear()
        Dim words = New List(Of String)
        For Each line In RtBox.Lines.Where(Function(l) Not String.IsNullOrEmpty(l))
            words.Add(Regex.Replace(line.Trim, " +", " "))
        Next
        voc.Words.AddRange(words.Distinct)
    End Sub

    ''' <summary>
    ''' Format: 
    '''    MainTerm: synonym1,synonym2, etc
    ''' </summary>
    Public Sub UpdateVocabularySynonyms(voc As Vocabulary)
        voc.Synonyms.Clear()
        For Each line In RtBox.Lines.Where(Function(l) Not String.IsNullOrEmpty(l))
            For Each m As Match In Regex.Matches(Regex.Replace(line, " +", " "), _patterns.Synonyms)
                Dim syn = New Vocabulary.Synonym(m.Groups(1).Value)
                For Each m1 As Match In Regex.Matches(m.Groups(2).Value, $"([^,]+)")
                    syn.Terms.Add(m1.Groups(1).Value.Trim)
                Next
                voc.Synonyms.Add(syn)
            Next
        Next
    End Sub

    ''' <summary>
    ''' Format:  filler-term : colloquial (optional)
    ''' </summary>
    Public Sub UpdateVocabularyRendering(voc As Vocabulary)
        voc.Renderings.Clear()
        For Each line In RtBox.Lines
            For Each m As Match In Regex.Matches(Regex.Replace(line, " +", " "), _patterns.Rendering)
                Dim rendering = New Vocabulary.Rendering(m.Groups(PatternGroups.Term.ToString).Value.Trim,
                                                         m.Groups(PatternGroups.Filler.ToString).Value.Trim, m.Groups(PatternGroups.Colloquial.ToString).Value.Trim)
                voc.Renderings.Add(rendering)
            Next
        Next
    End Sub

    Public Sub UpdateVocabularyList(formats As String)
        For Each m As Match In Regex.Matches(formats, $"\b[\w]+\b", RegexOptions.IgnoreCase)
            If Not _vocabularies.Any(Function(v) v.Name.ToLower = m.Value.Trim.ToLower) Then
                _vocabularies.Add(New Vocabulary(m.Value.Trim))
            End If
        Next
        Vocabularies = _vocabularies 'force refresh 
    End Sub

    Private Sub Menu(row As DataGridViewRow)
        Dim menu = New ToolStripDropDownMenu
        Dim mEdit = New ToolStripMenuItem("Edit", My.Resources.edit, Sub()
                                                                         GrdVocabularies.BeginEdit(True)
                                                                     End Sub)

        Dim mDelete = New ToolStripMenuItem("Delete", My.Resources.delete,
                                            Sub()
                                                _vocabularies.Remove(DirectCast(row.Tag, Vocabulary))
                                                GrdVocabularies.Rows.Remove(row)
                                            End Sub)
        Dim mClear = New ToolStripMenuItem("Clear All", My.Resources.clearall,
                                            Sub()
                                                _vocabularies.Clear()
                                                GrdVocabularies.Rows.Clear()
                                            End Sub)

        Dim mCopyWords = New ToolStripMenuItem("Copy Words", My.Resources.CopyToClipboard)

        Dim mSelectedVoc = New ToolStripMenuItem("Selected Vocabulary", My.Resources.Vocabulary,
                                                 Sub()
                                                     Dim sb = New StringBuilder
                                                     For Each w In _selectedVoc.Words
                                                         sb.AppendLine(w)
                                                     Next
                                                     Clipboard.SetText(sb.ToString)
                                                 End Sub)
        mSelectedVoc.ToolTipText = "Copy all the words of the selected vocabulary into the clipboard"

        Dim mAllVocs = New ToolStripMenuItem("All Vocabularies", My.Resources.Vocabulary,
                                                 Sub()
                                                     Dim sb = New StringBuilder
                                                     For Each w In _vocabularies.SelectMany(Function(voc) voc.Words)
                                                         sb.AppendLine(w)
                                                     Next
                                                     Clipboard.SetText(sb.ToString)
                                                 End Sub)
        mAllVocs.ToolTipText = "Copy all the words of the all the vocabularies into the clipboard"

        mCopyWords.DropDownItems.Add(mSelectedVoc)
        mCopyWords.DropDownItems.Add(mAllVocs)

        menu.Items.Add(mEdit)
        menu.Items.Add("-")
        menu.Items.Add(mDelete)
        menu.Items.Add(mClear)
        menu.Items.Add(mCopyWords)

        menu.Show(MousePosition)
    End Sub

    Private Sub GrdVocabularies_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles GrdVocabularies.CellMouseDown
        If e.Button = MouseButtons.Right AndAlso e.RowIndex > -1 Then
            GrdVocabularies.CurrentCell = GrdVocabularies.Rows(e.RowIndex).Cells(0)
            Menu(GrdVocabularies.CurrentRow)
        End If
    End Sub

    Private Sub GrdVocabularies_MouseWheel(sender As Object, e As MouseEventArgs) Handles GrdVocabularies.MouseWheel
        GrdVocabularies.StepFont(e.Delta)
        GrdVocabularies.DefaultCellStyle.Font = GrdVocabularies.Font
    End Sub

    Private Sub GrdVocabularies_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles GrdVocabularies.CellEndEdit
        Dim voc = DirectCast(GrdVocabularies.Rows(e.RowIndex).Tag, Vocabulary)
        Dim oldLabel = voc.Name
        Dim newLabel = GrdVocabularies.Rows(e.RowIndex).Cells(ColName.Index).Value.ToString
        voc.Name = newLabel
        RaiseEvent _VoceNameChanged(oldLabel, newLabel)
    End Sub

    Private Sub RTClipBoard1_TextChanged(sender As Object, e As EventArgs) Handles RTClipBoard1.TextChanged
        SplitWords.Panel2Collapsed = String.IsNullOrEmpty(RTClipBoard1.Text.Trim) AndAlso
            String.IsNullOrEmpty(RTClipBoard2.Text.Trim)
    End Sub

    Private Sub RTClipBoard2_TextChanged(sender As Object, e As EventArgs) Handles RTClipBoard2.TextChanged
        SplitWords.Panel2Collapsed = String.IsNullOrEmpty(RTClipBoard1.Text.Trim) AndAlso
            String.IsNullOrEmpty(RTClipBoard2.Text.Trim)
    End Sub

    Private Sub BuildToolstrip()
        Dim mBox = New ToolStripTextBox
        mBox.BorderStyle = BorderStyle.FixedSingle
        mBox.AutoSize = False
        mBox.Width = 200
        mBox.ToolTipText = "Write here the nane of the vocabulary to add"
        Dim mAdd = New ToolStripButton("Add", My.Resources.VocabularyEmpty)
        mAdd.ToolTipText = "Add the vocabulary"

        Dim mClipboard = New ToolStripButton("", My.Resources.CopyToClipboard)
        mClipboard.ToolTipText = "Show/Hide the clipboard"
        mClipboard.CheckOnClick = True

        Dim mWords = New ToolStripButton("W")
        mWords.ToolTipText = "Show the list of words contained in the vocabulary"

        Dim mSynonyms = New ToolStripButton("S")
        mSynonyms.ToolTipText = "Show the list of synonyms contained in the vocabulary"

        Dim mRendering = New ToolStripButton("R")
        mRendering.ToolTipText = "Show how vocabulary terms should be read back"

        Dim mCheck = New ToolStripButton("", My.Resources.CheckVocabularies)
        mCheck.CheckOnClick = True
        mRendering.ToolTipText = "Check duplicated words"

        AddHandler mWords.Click, Sub()
                                     DisplayState = DisplayStates.Words
                                     mWords.Checked = True
                                     mSynonyms.Checked = False
                                     mRendering.Checked = False
                                 End Sub

        AddHandler mSynonyms.Click, Sub()
                                        DisplayState = DisplayStates.Synonyms
                                        mWords.Checked = False
                                        mSynonyms.Checked = True
                                        mRendering.Checked = False
                                    End Sub

        AddHandler mRendering.Click, Sub()
                                         DisplayState = DisplayStates.Renderings
                                         mWords.Checked = False
                                         mSynonyms.Checked = False
                                         mRendering.Checked = True
                                     End Sub
        AddHandler mBox.KeyDown, Sub(s, e)
                                     If e.KeyCode = Keys.Return Then
                                         _vocabularies.RaiseListChangedEvents = False
                                         AddVocabulary(mBox.Text)
                                         _vocabularies.RaiseListChangedEvents = True
                                         mBox.Text = ""
                                     End If
                                 End Sub

        AddHandler mAdd.Click, Sub()
                                   AddVocabulary(mBox.Text)
                               End Sub

        AddHandler mClipboard.CheckedChanged, Sub()
                                                  SplitWords.Panel2Collapsed = Not mClipboard.Checked
                                              End Sub


        AddHandler mCheck.CheckedChanged, Sub() ShowCheck(mCheck.Checked)

        ToolStrip.ForeColor = Color.Black

        ToolStrip.Items.Add(mBox)
        ToolStrip.Items.Add(mAdd)
        ToolStrip.Items.Add(New ToolStripLabel("  "))  'as separator 
        ToolStrip.Items.Add(mWords)
        ToolStrip.Items.Add(mSynonyms)
        ToolStrip.Items.Add(mRendering)
        ToolStrip.Items.Add(New ToolStripLabel("  "))  'as separator
        ToolStrip.Items.Add(mClipboard)
        ToolStrip.Items.Add(mCheck)
        mWords.PerformClick()
    End Sub

    Private Sub CheckVocabularies()
        Dim rows = New List(Of DataGridViewRow)
        Dim multiVocWords = New List(Of String)
        For Each word In _vocabularies.SelectMany(Function(v) v.AllWords).OrderBy(Function(w) w)
            Dim vocs = _vocabularies.Where(Function(v) v.AllWords.Contains(word))
            If vocs.Count > 1 AndAlso Not multiVocWords.Contains(word) Then
                multiVocWords.Add(word)
                Dim row = New DataGridViewRow
                row.CreateCells(GrdCheck)
                Dim sb = New StringBuilder
                Dim i = 0
                For Each voc In vocs.OrderBy(Function(v) v.Name)
                    sb.Append($"{If(i = 0, voc.Name, $", {voc.Name}")}")
                    i += 1
                Next
                row.Cells(ColWord.Index).Value = word
                row.Cells(ColIconCheck.Index).Value = My.Resources.Vocabulary
                row.Cells(ColVocs.Index).Value = sb.ToString
                rows.Add(row)
            End If
        Next
        GrdCheck.Rows.Clear()
        GrdCheck.Rows.AddRange(rows.ToArray)
    End Sub

    Private Sub ShowCheck(show As Boolean)
        If show Then CheckVocabularies()
        SplitMain.Panel2Collapsed = Not show
    End Sub

    Private Sub RTClipBoard1_KeyDown(sender As Object, e As KeyEventArgs) Handles RTClipBoard1.KeyDown
        If e.KeyCode = Keys.X AndAlso ModifierKeys.HasFlag(Keys.Control) Then
            RTClipBoard2.Text = RTClipBoard2.Text & Environment.NewLine & RTClipBoard1.SelectedText
        End If
    End Sub

    Private Sub MenuWordList()
        Dim menu = New ToolStripDropDownMenu
        Dim mLoad = New ToolStripMenuItem("Load Missing Terms", Nothing,
                                          Sub()
                                              Dim missingTerms = _selectedVoc.Words.ToList
                                              For Each line In RtBox.Lines
                                                  For Each m As Match In Regex.Matches(line, _patterns.Rendering)
                                                      missingTerms.Remove(m.Groups(PatternGroups.Term).Value)
                                                  Next
                                              Next
                                              If RtBox.Text.Length = 0 Then
                                                  RtBox.Text = missingTerms.SerializeWords
                                              Else
                                                  RtBox.Text = $"{RtBox.Text}{Chr(13)}{missingTerms.SerializeWords}"
                                              End If
                                          End Sub)
        mLoad.ToolTipText = "Load all the missing terms of the vocabulary"

        Dim mClean = New ToolStripMenuItem("Remove Untagged Terms", My.Resources.clearall,
                                           Sub()
                                               Dim sb = New StringBuilder
                                               For Each line In RtBox.Lines
                                                   If Regex.Matches(line, "[-:]").Count > 0 Then
                                                       sb.AppendLine(line)
                                                   End If
                                               Next
                                               RtBox.Text = sb.ToString
                                           End Sub)

        Select Case DisplayState
            Case DisplayStates.Synonyms
                mLoad.ToolTipText = "Remove all words that have no defined synonyms"
            Case DisplayStates.Renderings
                mLoad.ToolTipText = "Remove all words that have no defined rendering"
        End Select

        menu.Items.Add(mLoad)
        menu.Items.Add(mClean)
        menu.Show(MousePosition)
    End Sub

    Private Sub MenuProcessWordList()
        Dim menu = New ToolStripDropDownMenu
        Dim mLow = New ToolStripMenuItem("All Lowercase", Nothing,
                                          Sub()
                                              RtBox.Text = RtBox.Text.ToLower
                                          End Sub)
        mLow.ToolTipText = "Set all the words in lowercase"

        Dim mUp = New ToolStripMenuItem("All uppercase", Nothing,
                                          Sub()
                                              RtBox.Text = RtBox.Text.ToUpper
                                          End Sub)
        mUp.ToolTipText = "Set all the words in uppercase"

        Dim mCapital = New ToolStripMenuItem("First Letter Capital", Nothing,
                                          Sub()
                                              Dim sb = New StringBuilder
                                              For Each line In RtBox.Lines.Where(Function(l) Not String.IsNullOrEmpty(l))
                                                  sb.AppendLine($"{line.Substring(0, 1).ToUpper}{line.Substring(1).ToLower}")
                                              Next
                                              RtBox.Text = sb.ToString
                                          End Sub)
        mUp.ToolTipText = "Capitalize the first letter of each term"

        menu.Items.Add(mLow)
        menu.Items.Add(mUp)
        menu.Items.Add(mCapital)
        menu.Show(MousePosition)
    End Sub

    Private Sub RtBox_MouseDown(sender As Object, e As MouseEventArgs) Handles RtBox.MouseDown
        If e.Button = MouseButtons.Right Then
            Select Case DisplayState
                Case DisplayStates.Synonyms, DisplayStates.Renderings
                    MenuWordList()
                Case DisplayStates.Words
                    MenuProcessWordList()
            End Select
        End If
    End Sub

    Private Sub RtBox_TextChanged(sender As Object, e As EventArgs) Handles RtBox.TextChanged
        If Not _ignore Then FormatText(_selectedVoc)
    End Sub
End Class
