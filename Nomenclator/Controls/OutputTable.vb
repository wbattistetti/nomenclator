﻿Imports System.ComponentModel
Imports System.Text
Imports System.Text.RegularExpressions

Public Class OutputTable
    Private Class RowView
        Public Property GridRow As DataGridViewRow
        Public Property GridRowIndex As Integer
        Public Property ItemText As String

        Private _prompts(Prompt.Types.Rendering) As String
        Public Property Prompts(col As Prompt.Types) As String
            Get
                Return _prompts(col)
            End Get
            Set(value As String)
                _prompts(col) = value
            End Set
        End Property

        Public Sub New(gridRow As DataGridViewRow, itemText As String)
            Me.GridRow = gridRow
            Me.ItemText = itemText
        End Sub
    End Class

    Private _specialColumnIndex(Prompt.Types.Rendering) As Integer
    Private _project As Project
    Private _rowViews As New List(Of RowView)
    Private _additionalFields As New List(Of Nomenclature.Field)
    Private _mCounter As ToolStripMenuItem
    Private _selectedTextBeforeChange As String = Nothing
    Private _editingCellText As String
    Private WithEvents _editCellBox As TextBox

    Public WriteOnly Property Project As Project
        Set(value As Project)
            _project = value
        End Set
    End Property

    Public Sub Save()
        _project.Prompts.Clear()
        For Each r In _rowViews.Select(Function(rv) rv.GridRow)
            If Not String.IsNullOrEmpty(r.Cells(Prompt.Types.StartAutomatic).Value?.ToString()) Then
                Dim prompt = New Prompt
                For Each type In System.Enum.GetValues(GetType(Prompt.Types))
                    prompt.Texts(CInt(type)) = r.Cells(CInt(type)).Value?.ToString()
                Next
                _project.Prompts.Add(prompt)
            End If
        Next
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Private Sub ShowItems()
        Me.Cursor = Cursors.WaitCursor
        Grd.EndEdit()
        Grd.Rows.Clear()
        Dim cachedRows = New List(Of RowView)
        cachedRows.AddRange(_rowViews)
        _rowViews = New List(Of RowView)
        Grd.Columns.Clear()

        If _project.Nomenclature Is Nothing Then Exit Sub
        SetGrid()
        BuildToolstripComplete()
        If Not _project.Nomenclature.Fields.Any Then Exit Sub
        Dim rows = New List(Of DataGridViewRow)
        Dim row = New DataGridViewRow
        Dim values = GetSourceValues()

        values = GetExpansions(values)
        Dim firstAddtionalFieldColIndex = -1
        For Each value In values.OrderBy(Function(v) v.Text)
            row = New DataGridViewRow
            row.Cells.Clear()
            For Each promptType As Prompt.Types In [Enum].GetValues(GetType(Prompt.Types))
                Dim rowCell As DataGridViewCell = Nothing
                'Select Case promptType
                '    Case Prompt.Types.Item, Prompt.Types.StartAutomatic
                '        rowCell = New DataGridViewTextBoxCell
                '    Case Else
                '        Dim comboCell = New DataGridViewComboBoxCell
                '        rowCell = comboCell
                '        comboCell.FlatStyle = FlatStyle.Flat
                '        comboCell.DisplayStyleForCurrentCellOnly = True
                'End Select
                rowCell = New DataGridViewTextBoxCell
                row.Cells.Add(rowCell)
            Next

            Dim rv = New RowView(row, value.Text)
            rv.Prompts(Prompt.Types.Item) = value.Text
            _rowViews.Add(rv)

            row.Cells(Prompt.Types.Item).Value = value.Text

            Dim prmpt = _project.Prompts.SingleOrDefault(Function(p) p.Texts(Prompt.Types.Item) = value.Text)
            If prmpt IsNot Nothing Then
                For Each type As Prompt.Types In System.Enum.GetValues(GetType(Prompt.Types))
                    If type <> Prompt.Types.Item AndAlso type <> Prompt.Types.StartAutomatic Then
                        row.Cells(CInt(type)).Value = prmpt.Texts(CInt(type))
                        rv.Prompts(type) = prmpt.Texts(CInt(type))
                    End If
                Next
            End If

            Dim cachedRow = cachedRows.SingleOrDefault(Function(cr) cr.ItemText = value.Text)
            If cachedRow IsNot Nothing Then
                For Each type As Prompt.Types In System.Enum.GetValues(GetType(Prompt.Types))
                    If type <> Prompt.Types.Item AndAlso type <> Prompt.Types.StartAutomatic Then
                        row.Cells(CInt(type)).Value = cachedRow.Prompts(type)
                        rv.Prompts(type) = cachedRow.Prompts(type)
                    End If
                Next
            End If

            If value.AdditionalValues.Any Then
                Dim i = 0
                For Each field In _additionalFields
                    row.Cells(CInt(Prompt.Types.Rendering) + 1 + i).Value = value.AdditionalValues(i)
                    i += 1
                Next
            End If
            rows.Add(row)

        Next
        With Grd
            .SuspendLayout()
            .Rows.AddRange(rows.ToArray)
            For Each rv In _rowViews
                rv.GridRowIndex = rv.GridRow.Index
            Next
            .ResumeLayout()
            _mCounter.Text = $"# {_rowViews.Count} items"
            _mCounter.Visible = True
        End With
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub SetGrid()
        Grd.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        For Each c As Prompt.Types In System.Enum.GetValues(GetType(Prompt.Types))
            Dim col = New DataGridViewTextBoxColumn
            col.Resizable = DataGridViewTriState.True
            col.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            col.HeaderText = c.ToString ' EnumUtils(Of Columns).GetDescription(c)
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            col.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            col.SortMode = DataGridViewColumnSortMode.Automatic ' if sort collumn the fixed row lost their position
            col.Tag = c
            Select Case c
                Case Prompt.Types.StartAutomatic
                    col.DefaultCellStyle.ForeColor = Color.LightSeaGreen
                Case Prompt.Types.StartManual
                    col.DefaultCellStyle.ForeColor = Color.LightGreen
                Case Prompt.Types.NoMatch1
                    col.DefaultCellStyle.ForeColor = Color.Coral
                Case Prompt.Types.NoMatch2
                    col.DefaultCellStyle.ForeColor = Color.Red
                Case Prompt.Types.Rendering
                    col.DefaultCellStyle.ForeColor = Color.LightBlue
            End Select
            Grd.Columns.Add(col)

            _specialColumnIndex(c) = col.Index
        Next
        AddAdditionalFieldColumns()
    End Sub

    Private Sub AddAdditionalFieldColumns()
        For Each field In _additionalFields
            Dim col = New DataGridViewTextBoxColumn
            col.Resizable = DataGridViewTriState.True
            col.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            col.HeaderText = field.Name
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            col.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            col.SortMode = DataGridViewColumnSortMode.Automatic ' if sort collumn the fixed row lost their position
            col.Tag = field
            Grd.Columns.Add(col)
        Next
    End Sub

    Public Sub BuildToolstripStart()
        ToolStrip.ForeColor = Color.Black

        For Each field In _project.Nomenclature.Fields.Except({_project.ParsedField})
            Dim tField = field
            Dim mField = New ToolStripButton(field.Name.ToUpper)
            mField.CheckOnClick = True
            mField.Checked = _additionalFields.Contains(tField)
            mField.ToolTipText = "Include/Exclude this field in the output"
            ToolStrip.Items.Add(mField)
            AddHandler mField.CheckStateChanged, Sub()
                                                     If mField.Checked Then
                                                         _additionalFields.Add(tField)
                                                     Else
                                                         _additionalFields.Remove(tField)
                                                     End If
                                                     ShowItems()
                                                 End Sub
        Next


        Dim mCreate = New ToolStripMenuItem("Create Prompts", Nothing,
                                            Sub()
                                                ShowItems()
                                                CreatePrompts()
                                            End Sub)
        mCreate.ToolTipText = "Create all the prompts"

        ToolStrip.Items.Add(mCreate)
    End Sub

    Private Sub BuildToolstripComplete()
        ToolStrip.Items.Clear()
        BuildToolstripStart()

        Dim mAll = New ToolStripMenuItem("All", Nothing, Sub()
                                                             ShowRows(True)
                                                         End Sub)
        mAll.ToolTipText = "Show all items"
        Dim mPromptOnly = New ToolStripButton("Prompts Only")

        AddHandler mPromptOnly.CheckStateChanged, Sub()
                                                      ShowRows(Not mPromptOnly.Checked)
                                                  End Sub
        mPromptOnly.CheckOnClick = True
        mPromptOnly.ToolTipText = "Show only the items that need a desambiguation prompt"
        _mCounter = New ToolStripMenuItem("")
        _mCounter.Visible = False

        ToolStrip.Items.Add(mAll)
        ToolStrip.Items.Add(mPromptOnly)
        ToolStrip.Items.Add("-")
        MenuColumnsButtons()
        MenuAdditionalFieldsButtons()
        ToolStrip.Items.Add("-")
        ToolStrip.Items.Add(_mCounter)
    End Sub

    Private Sub ShowRows(all As Boolean)
        Dim rvs As IEnumerable(Of RowView) = Nothing
        If Not all Then
            rvs = _rowViews.Where(Function(r) Not String.IsNullOrEmpty(r.GridRow.Cells(Prompt.Types.StartAutomatic).Value?.ToString))
        Else
            rvs = _rowViews
        End If
        Grd.SuspendLayout()
        Grd.Rows.Clear()
        Grd.Rows.AddRange(rvs.Select(Function(r) r.GridRow).ToArray)
        For Each rv In rvs
            rv.GridRowIndex = rv.GridRow.Index
        Next
        _mCounter.Text = $"# {rvs.Count} items"
        _mCounter.Visible = True
        Grd.ResumeLayout()
    End Sub


    ''' <remarks>
    ''' Given a set of values, for example:
    ''' Brown.Doctor.CardiacSurgery
    ''' Brown.Doctor.Orthopedics
    ''' Brown.Nurse.Midwife.BuildingA
    ''' Brown.Nurse.Midwife.BuildingAB
    ''' 
    ''' Auotmatically creates disambiguation prompts, for example.
    '''   Doctor or nurse?
    '''   
    ''' NOTE: the prommpt must be created based on the longest DIFFERENT descendants
    '''   
    ''' For example, if the seed is  "Brown.Nurse", given the two rows below:
    '''     Brown.Nurse.Midwife.BuildingA
    '''     Brown.Nurse.Midwife.BuildingAB
    '''     
    ''' The prompt must be:
    '''     Midwife.BuildingA or Midwife.BuildingB
    ''' </remarks>
    Private Sub CreatePrompts()
        _project.Prompts.Clear()
        Dim colInput = 0
        Dim rows = New List(Of DataGridViewRow)
        For Each row As DataGridViewRow In Grd.Rows
            rows.Add(row)
        Next
        For Each row In rows
            Dim initialSeed = row.Cells(colInput).Value?.ToString
            Dim seed = initialSeed
            Dim ambiguousRows = rows.Except({row}).
                Where(Function(r) Regex.Matches(r.Cells(colInput).Value.ToString, $"{seed}\.[^\.]+$").Count > 0)

            Do While ambiguousRows.Count = 1
                'extend the seed until only one row is found
                seed = ambiguousRows(0).Cells(colInput).Value.ToString
                ambiguousRows = rows.Except({row}).
                    Where(Function(r) Regex.Matches(r.Cells(colInput).Value.ToString, $"{seed}\.[^\.]+$").Count > 0)
            Loop

            If ambiguousRows.Count > 1 Then
                Dim sb = New StringBuilder
                Dim i = 0
                For Each r In ambiguousRows
                    Dim st = r.Cells(colInput).Value.ToString.Substring(initialSeed.Length + 1)
                    If i = 0 Then
                        sb.Append(st)
                    ElseIf i < ambiguousRows.Count - 1 Then
                        sb.Append($", {st}")
                    Else
                        sb.Append($" o {st}")
                    End If
                    i += 1
                Next
                If Not String.IsNullOrEmpty(sb.ToString) Then
                    row.Cells(Prompt.Types.StartAutomatic).Value = $"{sb.ToString}?"
                End If

            End If

            Dim rv = _rowViews.SingleOrDefault(Function(r) r.ItemText = initialSeed)
            row.Cells(Prompt.Types.StartManual).Value = _rowViews.SingleOrDefault(Function(r) r.ItemText = initialSeed)?.Prompts(Prompt.Types.StartManual)

            row.Cells(Prompt.Types.Rendering).Value = GetRendering(row.Cells(colInput).Value?.ToString)
        Next
        Grd.SuspendLayout()
        Dim rr = -1
        For Each row As DataGridViewRow In rows
            rr += 1
            Grd(Prompt.Types.StartAutomatic, rr).Value = row.Cells(Prompt.Types.StartAutomatic).Value
        Next
        Grd.ResumeLayout()
    End Sub

    Private Function GetRendering(parseResult As String) As String
        Dim itemRow = _project.Nomenclature.Rows.FirstOrDefault(Function(r) r.ParseResult = parseResult)
        If itemRow Is Nothing Then Return parseResult
        Dim cell = itemRow.Cells.Single(Function(c) c.FieldName = _project.ParsedField.Name)
        Dim sb = New StringBuilder
        If cell.WinnerClause Is Nothing Then Return parseResult
        For Each wordMatch In cell.WinnerClause.Words
            Dim voc = _project.Vocabularies.Single(Function(v) v.Name = wordMatch.VocName)  'mettere direttmsente wordmatch.voc
            Dim rendering = voc.Renderings.SingleOrDefault(Function(r) r.Term.ToLower = wordMatch.Value.ToLower)
            If rendering IsNot Nothing Then
                If Not String.IsNullOrEmpty(rendering.ColloquialTerm) Then
                    sb.Append($" {rendering.Filler} {rendering.ColloquialTerm }")
                Else
                    sb.Append($" {rendering.Filler} {rendering.Term}")
                End If
            Else
                sb.Append($" {wordMatch.Value}")
            End If
        Next
        Return Regex.Replace(sb.ToString.ToLower, " +", " ")
    End Function

    Private Function DotNameValues(texts As IEnumerable(Of String)) As IEnumerable(Of String)
        Return texts.Where(Function(v) v.Contains("."))
    End Function


    ''' <remarks>
    ''' Suppose to start with values:
    '''     Brown.Doctor.CardiacSurgery
    '''     Brown.Doctor.Orthopedics
    '''     Brown.Nurse.Midwife.BuildingA
    '''     Brown.Nurse.Midwife.BuildingB
    '''     
    ''' Expand with all the seeds that have more than one child.
    ''' In the example:
    '''     Brown
    '''     Brown.Doctor
    '''     Brown.Doctor.CardiacSurgery
    '''     Brown.Doctor.Orthopedics
    '''     Brown.Nurse.Midwife.BuildingA
    '''     Brown.Nurse.Midwife.BuildingB
    '''     
    '''   Note that "Brown.Nurse" is not necessary because it has
    ''' </remarks>
    ''' <param name="values"></param>
    ''' <returns></returns>
    Private Function GetExpansions(values As IEnumerable(Of SourceItem)) As IEnumerable(Of SourceItem)
        Dim newItems = New List(Of SourceItem)
        newItems.AddRange(values)
        For Each parsedText In values.Select(Function(v) v.ParseResult)
            Dim texts = GetSubStrings(parsedText)
            For Each st In texts
                If Not newItems.Any(Function(item) item.Text = st) Then newItems.Add(New SourceItem(st))
            Next
        Next
        Return newItems.Distinct
    End Function

    Private Function GetSourceValues() As IEnumerable(Of SourceItem)
        Dim sourceItems = New List(Of SourceItem)

        For Each itemRow In _project.Nomenclature.Rows.Where(Function(r) r.State = ParsingStates.Unique)
            Dim item = New SourceItem(itemRow.ParseResult)
            item.ParseResult = item.Text
            For Each field In _additionalFields
                item.AdditionalValues.Add(itemRow.Cells.Single(Function(c) c.FieldName = field.Name).Text)
            Next
            sourceItems.Add(item)
        Next
        Return sourceItems
    End Function

    Private Function GetSubStrings(text As String) As IEnumerable(Of String)
        Dim texts = New List(Of String)
        Dim sb = New StringBuilder
        For Each st In text.Split(".")
            sb.Append(If(String.IsNullOrEmpty(sb.ToString), st, $".{st}"))
            texts.Add(sb.ToString)
        Next
        If texts.Count = 1 Then Return {text} 'no ambiguity
        Return texts.Distinct
    End Function

    Private Sub Grd_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles Grd.CellBeginEdit
        Select Case e.ColumnIndex
            Case Prompt.Types.Item, Prompt.Types.StartAutomatic
                e.Cancel = True

            Case Else
                _editingCellText = Grd(e.ColumnIndex, e.RowIndex).Value?.ToString
        End Select
    End Sub

    Private Sub MenuColumnsButtons()
        Dim buttonsDict = New Dictionary(Of String, ToolStripButton)
        For Each c As Prompt.Types In System.Enum.GetValues(GetType(Prompt.Types))
            Dim col = Grd.GetColFromFieldName(c.ToString)
            Dim mButt = New ToolStripButton(col.HeaderText, Nothing)
            mButt.ToolTipText = "Show/Hide the column showing this field"
            buttonsDict.Add(c.ToString, mButt)
            mButt.CheckOnClick = True
            AddHandler mButt.CheckedChanged, Sub()
                                                 col.Visible = mButt.Checked
                                             End Sub
            mButt.Checked = col.Visible
            ToolStrip.Items.Add(mButt)
        Next
    End Sub

    Private Sub MenuAdditionalFieldsButtons()
        Dim buttonsDict = New Dictionary(Of String, ToolStripButton)
        For Each field In _additionalFields
            Dim col = Grd.GetColFromFieldName(field.Name)
            Dim mButt = New ToolStripButton(col.HeaderText, Nothing)
            mButt.ToolTipText = "Show/Hide the column showing this field"
            buttonsDict.Add(field.Name, mButt)
            mButt.CheckOnClick = True
            AddHandler mButt.CheckedChanged, Sub()
                                                 col.Visible = mButt.Checked
                                             End Sub
            mButt.Checked = col.Visible
            ToolStrip.Items.Add(mButt)
        Next
    End Sub

    Private Sub MenuCell(colIndex As Integer)
        Dim sb = New StringBuilder

        Dim menu = New ToolStripDropDownMenu
        menu.Items.Add("Copy Cell", My.Resources.CopyToClipboard,
                       Sub()
                           Clipboard.SetText(Grd.CurrentRow.Cells(colIndex).Value.ToString)
                       End Sub)

        menu.Items.Add("Copy Column", My.Resources.CopyToClipboard,
                       Sub()
                           sb = New StringBuilder
                           For Each row As DataGridViewRow In Grd.Rows
                               sb.AppendLine(row.Cells(colIndex).Value?.ToString.Trim)
                               'sb.AppendLine(Regex.Replace(row.Cells(colIndex).Value?.ToString.Trim, "\r\n", ""))
                           Next
                           Clipboard.SetText(sb.ToString)
                       End Sub)

        Dim mExportTable = New ToolStripMenuItem("Export Table", Nothing)

        Dim mBox = New ToolStripTextBox
        mBox.ToolTipText = "Write all the fields of the destination table separated by commas."
        mBox.BorderStyle = BorderStyle.FixedSingle
        mBox.AutoSize = False
        mBox.Width = 500
        If String.IsNullOrEmpty(_project.ExportFields) Then
            Dim i = 0
            For Each type As Prompt.Types In System.Enum.GetValues(GetType(Prompt.Types))
                sb.Append(If(i > 0, $", {type.ToString}", type.ToString))
                i += 1
            Next
            mBox.Text = sb.ToString
        Else
            mBox.Text = _project.ExportFields
        End If

        mExportTable.DropDownItems.Add(New ToolStripLabel("Table Fields:"))
        mExportTable.DropDownItems.Add(mBox)
        mExportTable.DropDownItems.Add("-")
        mExportTable.DropDownItems.Add("Copy Table To Clipboard", My.Resources.CopyToClipboard,
                                     Sub(s, e)
                                         _project.ExportFields = mBox.Text.Trim
                                         sb = New StringBuilder
                                         Dim col As DataGridViewColumn
                                         For Each row As DataGridViewRow In Grd.Rows
                                             Dim sbRow = New StringBuilder
                                             Dim i = 0
                                             For Each m As Match In Regex.Matches(mBox.Text, "[^,;]+")
                                                 If ExtraField(m.Value) Then
                                                     sbRow.Append(If(i = 0, " ", $", "))
                                                 Else
                                                     Dim cellText = row.Cells(GetColumnIndex(m.Value)).Value?.ToString.Trim
                                                     sbRow.Append(If(i = 0, $"{cellText} ", $",{cellText} "))
                                                 End If
                                                 i += 1
                                             Next
                                             sb.AppendLine(sbRow.ToString)
                                         Next
                                         Clipboard.SetText(sb.ToString)
                                         mBox.Owner.Dispose()
                                     End Sub)
        menu.Items.Add(mExportTable)

        menu.Show(MousePosition)
    End Sub

    Private Function GetColumnIndex(header As String) As Integer
        For Each col As DataGridViewColumn In Grd.Columns
            If col.HeaderText.ToLower = header.Trim.ToLower Then Return col.Index
        Next
        Return -1
    End Function

    Private Function ExtraField(name As String) As Boolean
        For Each type As Prompt.Types In System.Enum.GetValues(GetType(Prompt.Types))
            If name.ToLower.Trim = type.ToString.ToLower Then Return False
        Next
        Return True
    End Function

    Private Sub Grd_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles Grd.CellMouseDown
        If e.Button = MouseButtons.Right Then
            MenuCell(e.ColumnIndex)
        End If
    End Sub

    Private Sub Grd_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles Grd.EditingControlShowing
        _editCellBox = DirectCast(e.Control, TextBox)
        If Grd.CurrentCell.ColumnIndex = Prompt.Types.StartManual Then
            _editCellBox.SelectionStart = 0
            _editCellBox.SelectionLength = 0
        End If
        Exit Sub
        Dim editor = DirectCast(e.Control, ComboBox)


        editor.DropDownStyle = ComboBoxStyle.DropDown
        editor.Items.Clear()
        editor.Items.AddRange(Grd.Rows.Cast(Of DataGridViewRow).
                            Select(Function(r) r.Cells(Grd.CurrentCell.ColumnIndex).Value).Except({Nothing}).Concat({""}).Distinct.OrderBy(Function(v) v).ToArray)
    End Sub

    Private Sub Grd_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles Grd.CellEndEdit
        CompleteSimilarCells(e.ColumnIndex)
    End Sub

    Private Sub CompleteSimilarCells(colIndex As Integer)
        Select Case colIndex
            Case CInt(Prompt.Types.Item)
            Case CInt(Prompt.Types.Rendering)
                If String.IsNullOrEmpty(_selectedTextBeforeChange) Then Exit Sub
                Dim stBefore = ""
                Dim stAfter = ""
                Dim substitutionText = ""
                Dim newCellText = Grd.CurrentCell.Value.ToString
                Dim mcoll = Regex.Matches(_editingCellText, _selectedTextBeforeChange, RegexOptions.IgnoreCase)
                If mcoll.Count > 0 Then
                    stBefore = _editingCellText.Substring(0, mcoll(0).Index)
                    stAfter = _editingCellText.Substring(mcoll(0).Index + mcoll(0).Value.Length)
                    substitutionText = newCellText.Substring(mcoll(0).Index, newCellText.Length - stAfter.Length - stBefore.Length)
                End If

                For Each row As DataGridViewRow In Grd.Rows
                    If row.Cells(CInt(Prompt.Types.Rendering)).Value?.ToString.Contains(_selectedTextBeforeChange) Then
                        row.Cells(CInt(Prompt.Types.Rendering)).Value = row.Cells(CInt(Prompt.Types.Rendering)).Value.ToString.Replace(_selectedTextBeforeChange, substitutionText, StringComparison.OrdinalIgnoreCase)
                    End If
                Next
                _selectedTextBeforeChange = Nothing

            Case Else
                Dim automaticText = Grd.CurrentRow.Cells(Prompt.Types.StartAutomatic).Value.ToString
                Dim manualText = ""
                If Grd.CurrentRow.Cells(colIndex).Value IsNot Nothing Then
                    manualText = Regex.Replace(Grd.CurrentRow.Cells(colIndex).Value?.ToString.SetCapitalLetter, " +", " ")
                End If
                Grd.CurrentRow.Cells(colIndex).Value = manualText
                    For Each row As DataGridViewRow In Grd.Rows
                    If row.Cells(Prompt.Types.StartAutomatic).Value?.ToString.ToLower.Trim = automaticText.ToLower.Trim AndAlso
               String.IsNullOrEmpty(row.Cells(colIndex).Value?.ToString) Then
                        row.Cells(colIndex).Value = manualText
                        _rowViews.Single(Function(rv) rv.GridRow.Index = row.Index).Prompts(DirectCast(colIndex, Prompt.Types)) = manualText

                    End If
                Next
                _rowViews.Single(Function(rv) rv.GridRow.Index = Grd.CurrentCell.RowIndex).Prompts(DirectCast(colIndex, Prompt.Types)) = manualText
        End Select
    End Sub


    Private Sub Grd_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles Grd.DataError
        If e.ColumnIndex = 0 Then
        End If
    End Sub

    Private Sub _editCellBox_MouseUp(sender As Object, e As MouseEventArgs) Handles _editCellBox.MouseUp
        _selectedTextBeforeChange = _editCellBox.SelectedText
    End Sub

    Private Sub _editCellBox_KeyUp(sender As Object, e As KeyEventArgs) Handles _editCellBox.KeyUp
        Select Case e.KeyCode
            Case Keys.Left, Keys.Right
                _selectedTextBeforeChange = _editCellBox.SelectedText
        End Select
    End Sub

    Private Sub Grd_MouseWheel(sender As Object, e As MouseEventArgs) Handles Grd.MouseWheel
        Grd.StepFont(e.Delta)
        If Not Control.ModifierKeys = Keys.Control Then Exit Sub
        Cursor.Current = Cursors.WaitCursor
        Dim rows = New List(Of DataGridViewRow)
        For Each row As DataGridViewRow In Grd.Rows
            rows.Add(row)
        Next
        Grd.SuspendLayout()
        Grd.Rows.Clear()
        For Each row In rows
            row.DefaultCellStyle.Font = Grd.Font
        Next

        Grd.Rows.AddRange(rows.ToArray)
        Grd.ResumeLayout()
        Cursor.Current = Cursors.Default
    End Sub
End Class