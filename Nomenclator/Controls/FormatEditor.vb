﻿Imports System.ComponentModel
Imports System.Text
Imports System.Text.RegularExpressions
Imports ScintillaNET

Public Class FormatEditor
    Private _project As Project
    Private _tooltip As New ToolTip
    Private _ignore As Boolean
    Private WithEvents _intellisense As Intellisense

    Private WithEvents _vocabularies As BindingList(Of Vocabulary)
    Public Property Project As Project
        Get
            Return _project
        End Get
        Set(value As Project)
            _project = value
            RTFormats.Clear()
            RTFormats.Visible = _project IsNot Nothing
            If _project Is Nothing Then
                Exit Property
            End If
            _vocabularies = _project.Vocabularies
            RTFormats.Text = _project.Formats
            'FormatText()
        End Set
    End Property

    Public Sub Clear()
        RTFormats.Clear()
    End Sub

    Private Enum EditStates
        Adding
        Editing
    End Enum

    Private _editState As EditStates

    Public Function GetFormatClauses(vocabularies As IEnumerable(Of Vocabulary)) As IEnumerable(Of FormatClause)
        Dim clauses = New List(Of FormatClause)
        For Each line In RTFormats.Lines
            Dim clause = New FormatClause
            clause.Text = line
            Dim text = line
            For Each m As Match In Regex.Matches(line, "([^\:]+):")
                clause.Prefix = m.Groups(1).Value
                text = Regex.Replace(text, "([^\:]+):", "")
            Next

            For Each m As Match In Regex.Matches(text, "[^\.]+")
                Dim voc = vocabularies.SingleOrDefault(Function(v) v.Name.ToLower = m.Value.Trim.ToLower)
                If voc IsNot Nothing Then clause.SortedVocs.Add(voc)
            Next
            clauses.Add(clause)
        Next
        Return clauses
    End Function

    Private _selectetFormatIndex As Integer
    Private _formats As List(Of String)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        RTFormats.ContextMenuStrip = New ContextMenuStrip
    End Sub

    Public Sub Update(oldvocLabel As String, newVocLabel As String)
        RTFormats.Text = Regex.Replace(RTFormats.Text, $"\b{oldvocLabel}\b", newVocLabel, RegexOptions.IgnoreCase)
    End Sub

    Private Sub MenuFormat(MousePosition As Point)
        Dim m = New ToolStripDropDownMenu
        For Each voc In _project.Vocabularies.OrderBy(Function(v) v.Name)
            Dim tVoc = voc
            Dim mitem = New ToolStripMenuItem(voc.Name)

            AddHandler mitem.Click,
                        Sub()
                            mitem.Checked = Not mitem.Checked
                            With RTFormats
                                If Not mitem.Checked Then
                                    .Text = Regex.Replace(.Text, $"\.{tVoc.Name}\b", "")
                                    .Text = Regex.Replace(.Text, $"\b{tVoc.Name}\b", "")
                                Else
                                    .SelectedText = If(String.IsNullOrEmpty(.Text.Trim), tVoc.Name, $".{tVoc.Name}")
                                    .Text = .Text.Trim
                                    .SelectionStart = .Text.Length
                                End If
                            End With
                            m.Show(MousePosition)
                        End Sub
            mitem.Enabled = Regex.Matches(RTFormats.Text, $"\b{tVoc.Name}\b").Count = 0
            mitem.Checked = Regex.Matches(RTFormats.Text, $"\b{tVoc.Name}\b").Count > 0
            m.Items.Add(mitem)
        Next
        m.Show(MousePosition)
    End Sub

    Public Sub FormatText()
        If _ignore Then Exit Sub

        _ignore = True
        With RTFormats
            .SuspendLayout()
            Dim s = .SelectionStart
            .SelectionStart = 0
            .SelectionLength = .Text.Length
            .SelectionColor = Color.Red
            For Each voc In _project.Vocabularies
                For Each m As Match In Regex.Matches(.Text, $"\b{voc.Name}\b", RegexOptions.IgnoreCase)
                    .SelectionStart = m.Index
                    .SelectionLength = m.Length
                    .SelectionColor = Color.LightGreen
                Next
            Next

            For Each m As Match In Regex.Matches(.Text, "\.")
                .SelectionStart = m.Index
                .SelectionLength = m.Length
                .SelectionColor = Color.White  'the dots
            Next

            For i = 0 To .Lines.Count - 1
                For Each m As Match In Regex.Matches(.Lines(i), "^[^:]+:")
                    .SelectionStart = RTFormats.GetFirstCharIndexFromLine(i) + m.Index
                    .SelectionLength = m.Length
                    .SelectionColor = Color.Yellow
                Next
            Next
            ResumeLayout()
            .Visible = True
            .SelectionStart = s
            .SelectionLength = 0
            .SelectionColor = Color.Red
        End With
        _ignore = False
    End Sub

    Private Sub Grd_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs)
        e.Cancel = e.RowIndex <> 0
    End Sub

    Private Sub RTFormats_MouseDown(sender As Object, e As MouseEventArgs) Handles RTFormats.MouseDown
        If e.Button = MouseButtons.Right Then
            RTFormats.ContextMenuStrip = New ContextMenuStrip
            MenuFormat(MousePosition)
        Else
            If ModifierKeys.HasFlag(Keys.Shift) Then ShowTooltip(New Point(e.X, e.Y))
        End If
    End Sub

    Private Sub ShowTooltip(point As Point)
        Dim index = RTFormats.GetCharIndexFromPosition(point)
        Dim sb = New StringBuilder
        For Each voc In _project.Vocabularies
            For Each m As Match In Regex.Matches(RTFormats.Text, voc.Name, RegexOptions.IgnoreCase)
                If index >= m.Index AndAlso index <= m.Index + m.Value.Length Then
                    For i = 0 To Math.Min(10, voc.Words.Count) - 1
                        Dim rendering = voc.Renderings.SingleOrDefault(Function(r) r.Term = voc.Words(i))
                        If rendering IsNot Nothing Then
                            Dim term = voc.Words(i)
                            If Not String.IsNullOrEmpty(rendering.ColloquialTerm) Then term = rendering.ColloquialTerm
                            sb.AppendLine($"{rendering.Filler} {term}")
                        Else
                            sb.AppendLine(voc.Words(i))
                        End If
                    Next
                End If
            Next
        Next
        _tooltip.Show(sb.ToString, RTFormats)
    End Sub

    Private Function PointedIndex(x As Integer, y As Integer) As Integer
        Return RTFormats.Text.Substring(0, RTFormats.GetCharIndexFromPosition(New Point(x, y))).Split(".").Count
    End Function

    Private Sub RTFormats_TextChanged(sender As Object, e As EventArgs) Handles RTFormats.TextChanged
        _project.Formats = RTFormats.Text
        If _intellisense Is Nothing Then _intellisense = New Intellisense(RTFormats, _project.Vocabularies.Select(Function(v) v.Name))
        FormatText()
    End Sub

    Private Sub RTFormats_LostFocus(sender As Object, e As EventArgs) Handles RTFormats.LostFocus
        'FormatText()
    End Sub

    Private Sub RTFormats_MouseUp(sender As Object, e As MouseEventArgs) Handles RTFormats.MouseUp
        _tooltip.Hide(RTFormats)
    End Sub

    Private Sub _vocabularies_ListChanged(sender As Object, e As ListChangedEventArgs) Handles _vocabularies.ListChanged
        FormatText()
    End Sub

    Private Sub RTFormats_DoubleClick(sender As Object, e As EventArgs) Handles RTFormats.DoubleClick
        ShowTooltip(MousePosition)
    End Sub

    Private Sub RTFormats_Click(sender As Object, e As EventArgs) Handles RTFormats.Click
        _tooltip.Hide(RTFormats)
    End Sub
End Class
