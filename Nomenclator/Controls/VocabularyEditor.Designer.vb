﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VocabularyEditor
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitVocs = New System.Windows.Forms.SplitContainer()
        Me.GrdVocabularies = New System.Windows.Forms.DataGridView()
        Me.ColIcon = New System.Windows.Forms.DataGridViewImageColumn()
        Me.ColName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SplitWords = New System.Windows.Forms.SplitContainer()
        Me.RtBox = New System.Windows.Forms.RichTextBox()
        Me.SplitClipboard = New System.Windows.Forms.SplitContainer()
        Me.RTClipBoard1 = New System.Windows.Forms.RichTextBox()
        Me.LblClip1 = New System.Windows.Forms.Label()
        Me.RTClipBoard2 = New System.Windows.Forms.RichTextBox()
        Me.LblClip2 = New System.Windows.Forms.Label()
        Me.ToolStrip = New System.Windows.Forms.ToolStrip()
        Me.SplitMain = New System.Windows.Forms.SplitContainer()
        Me.GrdCheck = New System.Windows.Forms.DataGridView()
        Me.ColWord = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColIconCheck = New System.Windows.Forms.DataGridViewImageColumn()
        Me.ColVocs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.SplitVocs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitVocs.Panel1.SuspendLayout()
        Me.SplitVocs.Panel2.SuspendLayout()
        Me.SplitVocs.SuspendLayout()
        CType(Me.GrdVocabularies, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitWords, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitWords.Panel1.SuspendLayout()
        Me.SplitWords.Panel2.SuspendLayout()
        Me.SplitWords.SuspendLayout()
        CType(Me.SplitClipboard, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitClipboard.Panel1.SuspendLayout()
        Me.SplitClipboard.Panel2.SuspendLayout()
        Me.SplitClipboard.SuspendLayout()
        CType(Me.SplitMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitMain.Panel1.SuspendLayout()
        Me.SplitMain.Panel2.SuspendLayout()
        Me.SplitMain.SuspendLayout()
        CType(Me.GrdCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitVocs
        '
        Me.SplitVocs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitVocs.Cursor = System.Windows.Forms.Cursors.VSplit
        Me.SplitVocs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitVocs.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitVocs.Location = New System.Drawing.Point(0, 0)
        Me.SplitVocs.Name = "SplitVocs"
        '
        'SplitVocs.Panel1
        '
        Me.SplitVocs.Panel1.Controls.Add(Me.GrdVocabularies)
        '
        'SplitVocs.Panel2
        '
        Me.SplitVocs.Panel2.Controls.Add(Me.SplitWords)
        Me.SplitVocs.Size = New System.Drawing.Size(1091, 613)
        Me.SplitVocs.SplitterDistance = 255
        Me.SplitVocs.TabIndex = 0
        '
        'GrdVocabularies
        '
        Me.GrdVocabularies.AllowUserToAddRows = False
        Me.GrdVocabularies.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.GrdVocabularies.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.GrdVocabularies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdVocabularies.ColumnHeadersVisible = False
        Me.GrdVocabularies.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColIcon, Me.ColName, Me.ColCount})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GrdVocabularies.DefaultCellStyle = DataGridViewCellStyle2
        Me.GrdVocabularies.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GrdVocabularies.Location = New System.Drawing.Point(0, 0)
        Me.GrdVocabularies.Name = "GrdVocabularies"
        Me.GrdVocabularies.RowHeadersVisible = False
        Me.GrdVocabularies.RowTemplate.Height = 25
        Me.GrdVocabularies.Size = New System.Drawing.Size(253, 611)
        Me.GrdVocabularies.TabIndex = 0
        '
        'ColIcon
        '
        Me.ColIcon.HeaderText = "Icon"
        Me.ColIcon.MinimumWidth = 20
        Me.ColIcon.Name = "ColIcon"
        Me.ColIcon.ReadOnly = True
        Me.ColIcon.Width = 20
        '
        'ColName
        '
        Me.ColName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColName.HeaderText = "Vocabularies"
        Me.ColName.Name = "ColName"
        '
        'ColCount
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.ColCount.DefaultCellStyle = DataGridViewCellStyle1
        Me.ColCount.HeaderText = "Count"
        Me.ColCount.MinimumWidth = 30
        Me.ColCount.Name = "ColCount"
        Me.ColCount.ReadOnly = True
        Me.ColCount.Width = 30
        '
        'SplitWords
        '
        Me.SplitWords.BackColor = System.Drawing.Color.Black
        Me.SplitWords.Cursor = System.Windows.Forms.Cursors.VSplit
        Me.SplitWords.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitWords.Location = New System.Drawing.Point(0, 0)
        Me.SplitWords.Name = "SplitWords"
        '
        'SplitWords.Panel1
        '
        Me.SplitWords.Panel1.Controls.Add(Me.RtBox)
        '
        'SplitWords.Panel2
        '
        Me.SplitWords.Panel2.Controls.Add(Me.SplitClipboard)
        Me.SplitWords.Size = New System.Drawing.Size(830, 611)
        Me.SplitWords.SplitterDistance = 417
        Me.SplitWords.TabIndex = 1
        '
        'RtBox
        '
        Me.RtBox.BackColor = System.Drawing.Color.Black
        Me.RtBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RtBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RtBox.ForeColor = System.Drawing.Color.White
        Me.RtBox.Location = New System.Drawing.Point(0, 0)
        Me.RtBox.Name = "RtBox"
        Me.RtBox.Size = New System.Drawing.Size(417, 611)
        Me.RtBox.TabIndex = 0
        Me.RtBox.Text = ""
        '
        'SplitClipboard
        '
        Me.SplitClipboard.Cursor = System.Windows.Forms.Cursors.HSplit
        Me.SplitClipboard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitClipboard.Location = New System.Drawing.Point(0, 0)
        Me.SplitClipboard.Name = "SplitClipboard"
        Me.SplitClipboard.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitClipboard.Panel1
        '
        Me.SplitClipboard.Panel1.Controls.Add(Me.RTClipBoard1)
        Me.SplitClipboard.Panel1.Controls.Add(Me.LblClip1)
        '
        'SplitClipboard.Panel2
        '
        Me.SplitClipboard.Panel2.Controls.Add(Me.RTClipBoard2)
        Me.SplitClipboard.Panel2.Controls.Add(Me.LblClip2)
        Me.SplitClipboard.Size = New System.Drawing.Size(409, 611)
        Me.SplitClipboard.SplitterDistance = 305
        Me.SplitClipboard.TabIndex = 2
        '
        'RTClipBoard1
        '
        Me.RTClipBoard1.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(49, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.RTClipBoard1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTClipBoard1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RTClipBoard1.ForeColor = System.Drawing.Color.White
        Me.RTClipBoard1.Location = New System.Drawing.Point(0, 23)
        Me.RTClipBoard1.Name = "RTClipBoard1"
        Me.RTClipBoard1.Size = New System.Drawing.Size(409, 282)
        Me.RTClipBoard1.TabIndex = 2
        Me.RTClipBoard1.Text = ""
        '
        'LblClip1
        '
        Me.LblClip1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LblClip1.Dock = System.Windows.Forms.DockStyle.Top
        Me.LblClip1.ForeColor = System.Drawing.Color.White
        Me.LblClip1.Location = New System.Drawing.Point(0, 0)
        Me.LblClip1.Name = "LblClip1"
        Me.LblClip1.Size = New System.Drawing.Size(409, 23)
        Me.LblClip1.TabIndex = 3
        Me.LblClip1.Text = "Clipboard 1"
        Me.LblClip1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'RTClipBoard2
        '
        Me.RTClipBoard2.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(49, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.RTClipBoard2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTClipBoard2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RTClipBoard2.ForeColor = System.Drawing.Color.White
        Me.RTClipBoard2.Location = New System.Drawing.Point(0, 23)
        Me.RTClipBoard2.Name = "RTClipBoard2"
        Me.RTClipBoard2.Size = New System.Drawing.Size(409, 279)
        Me.RTClipBoard2.TabIndex = 2
        Me.RTClipBoard2.Text = ""
        '
        'LblClip2
        '
        Me.LblClip2.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LblClip2.Dock = System.Windows.Forms.DockStyle.Top
        Me.LblClip2.ForeColor = System.Drawing.Color.White
        Me.LblClip2.Location = New System.Drawing.Point(0, 0)
        Me.LblClip2.Name = "LblClip2"
        Me.LblClip2.Size = New System.Drawing.Size(409, 23)
        Me.LblClip2.TabIndex = 4
        Me.LblClip2.Text = "Clipboard 2"
        Me.LblClip2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStrip
        '
        Me.ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip.Name = "ToolStrip"
        Me.ToolStrip.Size = New System.Drawing.Size(1091, 25)
        Me.ToolStrip.TabIndex = 1
        Me.ToolStrip.Text = "ToolStrip1"
        '
        'SplitMain
        '
        Me.SplitMain.Cursor = System.Windows.Forms.Cursors.HSplit
        Me.SplitMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitMain.Location = New System.Drawing.Point(0, 25)
        Me.SplitMain.Name = "SplitMain"
        Me.SplitMain.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitMain.Panel1
        '
        Me.SplitMain.Panel1.Controls.Add(Me.SplitVocs)
        '
        'SplitMain.Panel2
        '
        Me.SplitMain.Panel2.Controls.Add(Me.GrdCheck)
        Me.SplitMain.Panel2.Controls.Add(Me.Label1)
        Me.SplitMain.Size = New System.Drawing.Size(1091, 723)
        Me.SplitMain.SplitterDistance = 613
        Me.SplitMain.TabIndex = 2
        '
        'GrdCheck
        '
        Me.GrdCheck.AllowUserToAddRows = False
        Me.GrdCheck.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.GrdCheck.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.GrdCheck.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.GrdCheck.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdCheck.ColumnHeadersVisible = False
        Me.GrdCheck.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColWord, Me.ColIconCheck, Me.ColVocs})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GrdCheck.DefaultCellStyle = DataGridViewCellStyle4
        Me.GrdCheck.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GrdCheck.Location = New System.Drawing.Point(0, 23)
        Me.GrdCheck.Name = "GrdCheck"
        Me.GrdCheck.RowHeadersVisible = False
        Me.GrdCheck.RowTemplate.Height = 25
        Me.GrdCheck.Size = New System.Drawing.Size(1091, 83)
        Me.GrdCheck.TabIndex = 3
        '
        'ColWord
        '
        Me.ColWord.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.ColWord.HeaderText = "Word"
        Me.ColWord.Name = "ColWord"
        Me.ColWord.Width = 5
        '
        'ColIconCheck
        '
        Me.ColIconCheck.HeaderText = "Icon"
        Me.ColIconCheck.MinimumWidth = 20
        Me.ColIconCheck.Name = "ColIconCheck"
        Me.ColIconCheck.ReadOnly = True
        Me.ColIconCheck.Width = 20
        '
        'ColVocs
        '
        Me.ColVocs.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ColVocs.DefaultCellStyle = DataGridViewCellStyle3
        Me.ColVocs.HeaderText = "Vocabularies"
        Me.ColVocs.MinimumWidth = 30
        Me.ColVocs.Name = "ColVocs"
        Me.ColVocs.ReadOnly = True
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(1091, 23)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Vocabularies Check"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'VocabularyEditor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.Controls.Add(Me.SplitMain)
        Me.Controls.Add(Me.ToolStrip)
        Me.Name = "VocabularyEditor"
        Me.Size = New System.Drawing.Size(1091, 748)
        Me.SplitVocs.Panel1.ResumeLayout(False)
        Me.SplitVocs.Panel2.ResumeLayout(False)
        CType(Me.SplitVocs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitVocs.ResumeLayout(False)
        CType(Me.GrdVocabularies, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitWords.Panel1.ResumeLayout(False)
        Me.SplitWords.Panel2.ResumeLayout(False)
        CType(Me.SplitWords, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitWords.ResumeLayout(False)
        Me.SplitClipboard.Panel1.ResumeLayout(False)
        Me.SplitClipboard.Panel2.ResumeLayout(False)
        CType(Me.SplitClipboard, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitClipboard.ResumeLayout(False)
        Me.SplitMain.Panel1.ResumeLayout(False)
        Me.SplitMain.Panel2.ResumeLayout(False)
        CType(Me.SplitMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitMain.ResumeLayout(False)
        CType(Me.GrdCheck, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents SplitVocs As SplitContainer
    Friend WithEvents GrdVocabularies As DataGridView
    Friend WithEvents RtBox As RichTextBox
    Friend WithEvents SplitWords As SplitContainer
    Friend WithEvents ToolStrip As ToolStrip
    Friend WithEvents SplitClipboard As SplitContainer
    Public WithEvents RTClipBoard1 As RichTextBox
    Public WithEvents RTClipBoard2 As RichTextBox
    Friend WithEvents LblClip1 As Label
    Friend WithEvents LblClip2 As Label
    Friend WithEvents ColIcon As DataGridViewImageColumn
    Friend WithEvents ColName As DataGridViewTextBoxColumn
    Friend WithEvents ColCount As DataGridViewTextBoxColumn
    Friend WithEvents SplitMain As SplitContainer
    Friend WithEvents GrdCheck As DataGridView
    Friend WithEvents ColWord As DataGridViewTextBoxColumn
    Friend WithEvents ColIconCheck As DataGridViewImageColumn
    Friend WithEvents ColVocs As DataGridViewTextBoxColumn
    Friend WithEvents Label1 As Label
End Class
