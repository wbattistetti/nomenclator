﻿Imports System.ComponentModel
Imports System.Reflection

Public Class EnumUtils(Of T)
    Public Shared Function GetDescription(enumValue As T, defDesc As String) As String

        Dim fi As FieldInfo = enumValue.[GetType]().GetField(enumValue.ToString())

        If fi IsNot Nothing Then
            Dim attrs As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), True)
            If attrs IsNot Nothing AndAlso attrs.Length > 0 Then
                Return DirectCast(attrs(0), DescriptionAttribute).Description
            End If
        End If

        Return defDesc
    End Function

    Public Shared Function GetDescription(enumValue As T) As String
        Return GetDescription(enumValue, enumValue.ToString)
    End Function

    Public Shared Function FromDescription(description As String) As T
        Dim t As Type = GetType(T)
        For Each fi As FieldInfo In t.GetFields()
            Dim attrs As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), True)
            If attrs IsNot Nothing AndAlso attrs.Length > 0 Then
                For Each attr As DescriptionAttribute In attrs
                    If attr.Description.Equals(description) Then
                        Return DirectCast(fi.GetValue(Nothing), T)
                    End If
                Next
            End If
        Next
        Return Nothing
    End Function
End Class