﻿Imports System.IO
Imports System.Xml

Public Module Saver
    Public Sub SaveProjectFile(project As Project, fileName As String)
        Try
            Dim settings As New XmlWriterSettings()
            settings.Indent = True
            'fileName = "c:\temp\HealthDetails.Nomenclature"
            'project.Path = "c:\temp\HealthDetails.Nomenclature"
            If Not Directory.Exists(Path.GetDirectoryName(fileName)) Then Directory.CreateDirectory(Path.GetDirectoryName(fileName))
            Dim xmlWrt As XmlWriter = XmlWriter.Create(fileName, settings)
            SaveProject(project, xmlWrt)
            xmlWrt.Close()
        Catch ex As Exception
            MessageBox.Show($"The file could not be saved:{Environment.NewLine}{fileName }{Environment.NewLine}{Environment.NewLine}Refer to log...",
                   "Saving Dialogue file...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SaveProject(prj As Project, xmlWrt As XmlWriter)
        With xmlWrt
            .WriteStartElement(NameOf(Project))
            If prj.ParsedField IsNot Nothing Then .WriteAttributeString(NameOf(Project.ParsedField), prj.ParsedField.Name)

            .WriteStartElement(NameOf(Project.Formats))
            .WriteCData(prj.Formats)
            .WriteEndElement()

            If Not String.IsNullOrEmpty(prj.ExportFields) Then
                .WriteStartElement(NameOf(Project.ExportFields))
                .WriteCData(prj.ExportFields)
                .WriteEndElement()
            End If

            SaveNomenclature(prj.Nomenclature, NameOf(prj.Nomenclature), xmlWrt)
            SaveVocabularies(prj.Vocabularies, xmlWrt)
            SavePrompts(prj.Prompts, xmlWrt)
            .WriteEndElement()
        End With

    End Sub

    Private Sub SaveNomenclature(nomenclature As Nomenclature, label As String, xmlWrt As XmlWriter)
        With xmlWrt
            .WriteStartElement(label)  'nomenclature

            .WriteStartElement(NameOf(nomenclature.Fields))
            For Each field In nomenclature.Fields
                .WriteStartElement(NameOf(Nomenclature.Field))
                .WriteAttributeString(NameOf(Nomenclature.Field.Name), field.Name)
                .WriteEndElement()
            Next
            .WriteEndElement() 'fields

            .WriteStartElement(NameOf(nomenclature.Rows))
            For Each row In nomenclature.Rows
                .WriteStartElement(NameOf(row))

                .WriteAttributeString(NameOf(ItemRow.State), row.State.ToString)

                If Not String.IsNullOrEmpty(row.ParseResult) Then
                    .WriteStartElement(NameOf(row.ParseResult))
                    .WriteCData(row.ParseResult)
                    .WriteEndElement() 'parseresult 
                End If
                .WriteStartElement(NameOf(row.Cells))

                For Each cell In row.Cells
                    .WriteStartElement(NameOf(cell))
                    .WriteAttributeString(NameOf(cell.FieldName), cell.FieldName)
                    .WriteCData(cell.Text)
                    .WriteEndElement() 'cell
                Next
                .WriteEndElement() 'cells
                .WriteEndElement() 'row 
            Next
            .WriteEndElement() 'rows

            .WriteEndElement() 'nomenclature
        End With
    End Sub

    Private Sub SaveVocabularies(vocabularies As IEnumerable(Of Vocabulary), xmlWrt As XmlWriter)
        With xmlWrt
            .WriteStartElement(NameOf(Project.Vocabularies))
            For Each voc In vocabularies.OrderBy(Function(v) v.Name)
                .WriteStartElement(NameOf(Vocabulary))
                .WriteAttributeString(NameOf(Vocabulary.Name), voc.Name)
                .WriteStartElement(NameOf(Vocabulary.Words))
                For Each word In voc.Words
                    .WriteStartElement(NameOf(word))
                    .WriteCData(word)
                    .WriteEndElement() 'word
                Next
                .WriteEndElement() 'words

                If voc.Synonyms.Any Then
                    .WriteStartElement(NameOf(voc.Synonyms))
                    For Each syn In voc.Synonyms
                        .WriteStartElement(NameOf(Vocabulary.Synonym))
                        .WriteAttributeString(NameOf(syn.MainTerm), syn.MainTerm)
                        For Each term In syn.Terms
                            .WriteStartElement("Term")
                            .WriteCData(term.Trim)
                            .WriteEndElement() 'term
                        Next
                        .WriteEndElement() 'synonym
                    Next
                    .WriteEndElement() 'synonyms
                End If

                If voc.Renderings.Any Then
                    .WriteStartElement(NameOf(voc.Renderings))
                    For Each rendering In voc.Renderings
                        .WriteStartElement(NameOf(Vocabulary.Rendering))
                        .WriteAttributeString(NameOf(rendering.Term), rendering.Term)
                        .WriteAttributeString(NameOf(rendering.Filler), rendering.Filler)
                        .WriteAttributeString(NameOf(rendering.ColloquialTerm), rendering.ColloquialTerm)
                        .WriteEndElement() 'rendering
                    Next
                    .WriteEndElement() 'rendering
                End If

                .WriteEndElement() 'voc
            Next
            .WriteEndElement() 'vocabularies
        End With
    End Sub

    Private Sub SavePrompts(prompts As IEnumerable(Of Prompt), xmlWrt As XmlWriter)
        With xmlWrt
            If prompts.Any Then
                .WriteStartElement(NameOf(Project.Prompts))
                For Each pr In prompts.Where(Function(p) Not String.IsNullOrEmpty(p.Texts(Prompt.Types.StartAutomatic)))
                    .WriteStartElement(NameOf(Prompt))
                    For Each type As Prompt.Types In System.Enum.GetValues(GetType(Prompt.Types))
                        If Not String.IsNullOrEmpty(pr.Texts(type)) Then
                            .WriteStartElement(type.ToString)
                            .WriteCData(pr.Texts(type))
                            .WriteEndElement() 'type
                        End If
                    Next
                    .WriteEndElement() 'prompt
                Next
                .WriteEndElement() 'prompts
            End If
        End With
    End Sub

End Module
