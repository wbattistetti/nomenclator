﻿Imports System.Runtime.CompilerServices
Imports System.Text
Imports System.Text.RegularExpressions

Public Module Utils
    Public Enum ParsingStates
        All
        Completed
        Unique
        Ambiguous
        UnMatched
        Excluded
        Underspecified
        ManuallyDesigned
    End Enum

    <Extension>
    Public Sub StepFont(control As Control, delta As Integer)
        If Not Control.ModifierKeys = Keys.Control Then Exit Sub
        Cursor.Current = Cursors.WaitCursor
        If delta > 0 Then
            control.Font = New Font(control.Font.FontFamily, Math.Min(control.Font.Size + 2, 20), FontStyle.Regular)
        Else
            control.Font = New Font(control.Font.FontFamily, Math.Max(control.Font.Size - 2, 8), FontStyle.Regular)
        End If
        Cursor.Current = Cursors.Default
    End Sub

    <Extension>
    Public Function GetWords(text As String, Optional minLength As Integer = 2) As IEnumerable(Of String)
        Dim words = New List(Of String)
        For Each m As Match In Regex.Matches(text, "[\wàèìòù]+")
            If m.Value.Length >= minLength Then words.Add(m.Value)
        Next
        Return words
    End Function

    <Extension>
    Public Function SerializeWords(texts As IEnumerable(Of String)) As String
        Dim sb = New StringBuilder
        For Each st In texts.OrderBy(Function(w) w)
            sb.AppendLine(st)
        Next
        Return sb.ToString
    End Function


    <Extension>
    Public Function GetColFromFieldName(grd As DataGridView, fieldname As String) As DataGridViewColumn
        For Each c As DataGridViewColumn In grd.Columns
            If c.HeaderText = fieldname Then Return c
        Next
        Return Nothing
    End Function

    <Extension>
    Public Function AllWords(voc As Vocabulary) As IEnumerable(Of String)
        Return voc.Words.
                   Concat(voc.Synonyms.Select(Function(s) s.MainTerm)).
                   Concat(voc.Synonyms.SelectMany(Function(s) s.Terms)).OrderBy(Function(w) w)
    End Function

    <Extension>
    Public Function SetCapitalLetter(text As String) As String
        If String.IsNullOrEmpty(text) Then Return ""
        Return $"{text.Substring(0, 1).ToUpper}{text.Substring(1).ToLower}"
    End Function
End Module
