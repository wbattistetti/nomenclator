﻿Imports System.Text.RegularExpressions

Public Class Intellisense
    Public Event _DotPressed()
    Public Event _KeyDown(keyCode As Integer)
    Public Event _KeyUp(keyCode As Integer)
    Public Property Words As IEnumerable(Of String)
    Private WithEvents _textBox As TextBoxBase
    Private _deletingChars As Boolean
    Private _ignore As Boolean

    Private Sub _textBox_TextChanged(sender As Object, e As EventArgs) Handles _textBox.TextChanged
        If _ignore OrElse _deletingChars OrElse Words Is Nothing OrElse Not Words.Any Then Exit Sub
        With _textBox
            Dim lineIndex = _textBox.GetLineFromCharIndex(.SelectionStart)
            Dim s = _textBox.SelectionStart
            'Dim seed = _textBox.Text.Substring(_textBox.GetFirstCharIndexFromLine(lineIndex), _textBox.SelectionStart - _textBox.GetFirstCharIndexFromLine(lineIndex)).Trim.Split(".").Last
            Dim seed = _textBox.Text.Substring(_textBox.GetFirstCharIndexFromLine(lineIndex), _textBox.SelectionStart - _textBox.GetFirstCharIndexFromLine(lineIndex)).Trim
            Dim mcoll = Regex.Matches(seed, "[^\.:]+")
            If mcoll.Count > 0 Then seed = mcoll(mcoll.Count - 1).Value

            If String.IsNullOrEmpty(seed) Then Exit Sub
            Dim word = Words.OrderBy(Function(w) w.Length).FirstOrDefault(Function(w) w.Substring(0, Math.Min(seed.Length, w.Length)).ToLower = seed.ToLower)
            If word IsNot Nothing Then
                Dim txt = word.Substring(seed.Length)
                _ignore = True
                .SelectedText = txt
                .SelectionStart = s
                .SelectionLength = txt.Length
                _ignore = False
            End If
        End With
    End Sub

    Public Sub New(textBox As TextBoxBase, words As IEnumerable(Of String))
        _textBox = textBox
        Me.Words = words
    End Sub

    Private Sub _textBox_KeyDown(sender As Object, e As KeyEventArgs) Handles _textBox.KeyDown
        Select Case e.KeyCode
            Case Keys.Back, Keys.Cancel
                _deletingChars = True

            Case Keys.OemPeriod
                _ignore = True
                RaiseEvent _DotPressed()
                _ignore = False

            Case Else
                _deletingChars = False
        End Select

        RaiseEvent _KeyDown(e.KeyCode)
    End Sub

    Private Sub _textBox_KeyUp(sender As Object, e As KeyEventArgs) Handles _textBox.KeyUp
        RaiseEvent _KeyUp(e.KeyCode)
    End Sub
End Class
