﻿Imports System.Runtime.CompilerServices
Imports System.Xml

Module Loader
    Private _project As Project
    Public Function LoadProjectFile(fileName As String) As Project
        Try
            If String.IsNullOrEmpty(fileName) Then Return Nothing
            Dim xmldoc As New XmlDocument()
            xmldoc.Load(fileName)
            Dim projectXml As XmlElement = DirectCast(xmldoc.SelectSingleNode(NameOf(Project)), XmlElement)
            Return LoadProject(fileName, projectXml)

        Catch ex As Exception
            MessageBox.Show($"Laading project. The file '{fileName }' could not be loaded")
        End Try
    End Function

    Private Function LoadProject(fileName As String, projectXml As XmlElement) As Project
        Try
            _project = New Project
            Dim xmlFormats = DirectCast(projectXml.SelectSingleNode(NameOf(Project.Formats)), XmlElement)
            If xmlFormats IsNot Nothing Then _project.Formats = xmlFormats.InnerText

            Dim xmlExportFields = DirectCast(projectXml.SelectSingleNode(NameOf(Project.ExportFields)), XmlElement)
            If xmlExportFields IsNot Nothing Then _project.ExportFields = xmlExportFields.InnerText

            LoadNomenclature(DirectCast(projectXml.SelectSingleNode(NameOf(Project.Nomenclature)), XmlElement), _project.Nomenclature)
            LoadVocabularies(DirectCast(projectXml.SelectSingleNode(NameOf(Project.Vocabularies)), XmlElement))
            LoadPrompts(DirectCast(projectXml.SelectSingleNode(NameOf(Project.Prompts)), XmlElement))

            If Not String.IsNullOrEmpty(projectXml.GetAttribute(NameOf(Project.ParsedField))) Then
                _project.ParsedField = _project.Nomenclature.Fields.Single(Function(f) f.Name = projectXml.GetAttribute(NameOf(Project.ParsedField)))
            End If

            _project.Path = fileName
            Return _project
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Function

    Private Sub LoadNomenclature(ontologyXml As XmlNode, ontology As Nomenclature)
        Try
            If ontologyXml IsNot Nothing Then
                For Each xmlField As XmlElement In ontologyXml.SelectSingleNode(NameOf(ontology.Fields)).ChildNodes
                    ontology.Fields.Add(New Nomenclature.Field(xmlField.GetAttribute(NameOf(Nomenclature.Field.Name))))
                Next

                For Each xmlRow As XmlElement In ontologyXml.SelectSingleNode(NameOf(ontology.Rows)).ChildNodes
                    Dim row = New ItemRow

                    Dim aState = xmlRow.GetAttribute(NameOf(ItemRow.State))
                    If Not String.IsNullOrEmpty(aState) Then
                        row.State = DirectCast([Enum].Parse(GetType(ParsingStates), aState), ParsingStates)
                    End If


                    Dim xmlParse = xmlRow.SelectSingleNode(NameOf(ItemRow.ParseResult))
                    If xmlParse IsNot Nothing Then
                        row.ParseResult = xmlParse.InnerText
                    End If

                    For Each xmlCell As XmlElement In xmlRow.SelectSingleNode(NameOf(ItemRow.Cells)).ChildNodes
                        row.Cells.Add(New ItemRow.Cell(xmlCell.GetAttribute(NameOf(ItemRow.Cell.FieldName)), xmlCell.InnerText))
                    Next
                    ontology.Rows.Add(row)
                Next
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadVocabularies(vocabulariesXml As XmlNode)
        Try
            If vocabulariesXml IsNot Nothing Then
                For Each xmlVoc As XmlElement In vocabulariesXml.ChildNodes
                    Dim voc = New Vocabulary(xmlVoc.GetAttribute(NameOf(Vocabulary.Name)))
                    For Each xmlWord As XmlElement In xmlVoc.SelectSingleNode(NameOf(Vocabulary.Words)).ChildNodes
                        voc.Words.Add(xmlWord.InnerText)
                    Next
                    Dim xmlSynonyms = xmlVoc.SelectSingleNode(NameOf(Vocabulary.Synonyms))
                    If xmlSynonyms IsNot Nothing Then
                        For Each xmlSynonym As XmlElement In xmlSynonyms.ChildNodes
                            Dim syn = New Vocabulary.Synonym(xmlSynonym.GetAttribute(NameOf(Vocabulary.Synonym.MainTerm)))
                            For Each xmlTerm As XmlElement In xmlSynonym.ChildNodes
                                syn.Terms.Add(xmlTerm.InnerText)
                            Next
                            voc.Synonyms.Add(syn)
                        Next
                    End If

                    Dim xmlRenderings = xmlVoc.SelectSingleNode(NameOf(Vocabulary.Renderings))
                    If xmlRenderings IsNot Nothing Then
                        For Each xmlRendering As XmlElement In xmlRenderings.ChildNodes
                            Dim rendering = New Vocabulary.Rendering(xmlRendering.GetAttribute(NameOf(Vocabulary.Rendering.Term)),
                                                                xmlRendering.GetAttribute(NameOf(Vocabulary.Rendering.Filler)),
                                                                xmlRendering.GetAttribute(NameOf(Vocabulary.Rendering.ColloquialTerm)))
                            voc.Renderings.Add(rendering)
                        Next
                    End If

                    _project.Vocabularies.Add(voc)
                Next
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadPrompts(promptsXml As XmlNode)
        Try
            If promptsXml IsNot Nothing Then
                For Each xmlPrompt As XmlElement In promptsXml.ChildNodes
                    Dim prompt = New Prompt
                    For Each xmlText As XmlElement In xmlPrompt.ChildNodes
                        Dim type = DirectCast([Enum].Parse(GetType(Prompt.Types), xmlText.Name), Prompt.Types)
                        prompt.Texts(type) = xmlText.InnerText
                    Next
                    _project.Prompts.Add(prompt)
                Next
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Module
